/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.model.AnnotationItem;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * [CDT-03], [CDT-04] e [CDT-06]
 *
 * @author Caps
 */
public class TableViewControllerTest extends Application {

    public static final String PATH = "C:\\Users\\Caps\\Documents\\CAS_events\\94c59136-3ad3-45bf-a201-28ef12c9a374";

    public TableViewControllerTest() {
        try {
            this.init();
            this.start(null);
        } catch (Exception ex) {
            Logger.getLogger(MediaPlayerCreatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of putData method, of class TableViewController.
     */
    @Test
    public void testPutData() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("putData");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                TableViewController instance = new TableViewController();
                instance.putData(desc);
            }
        });
    }

    /**
     * Test of handleNAddButtonAction method, of class TableViewController.
     */
    @Test
    public void testHandleNAddButtonAction() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("handleNAddButtonAction");
                ActionEvent event = null;
                TableViewController instance = new TableViewController();
                instance.getnStart().setText("000");
                instance.getnEnd().setText("500");
                instance.getnStart().setText("AnnotationTest");
                instance.handleNAddButtonAction(event);
            }
        });
    }

    /**
     * Test for editing text
     */
    @Test
    public void testTextEdit() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("TextEdit");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                TableViewController instance = new TableViewController();
                instance.putData(desc);
                int index = 0;
                AnnotationItem item = null;
                item = instance.getAnnotations().get(index);
                String newContent = "New Content";
                String newStart = "000";
                String newEnd = "500";
                item.setContent(newContent);
                item.setStart(newStart);
                item.setEnd(newEnd);
            }
        });
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    }

}
