/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.controller.MediaDescriptor;
import br.puc_rio.casmediaeditor.model.Annotation;
import br.puc_rio.casmediaeditor.model.Audio;
import br.puc_rio.casmediaeditor.model.Master;
import br.puc_rio.casmediaeditor.model.Video;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * [CDT01] [CDT-07]
 * @author Caps
 */
public class MediaDescriptorTest {
    
    public static final String PATH = "C:\\Users\\Caps\\Documents\\CAS_events\\94c59136-3ad3-45bf-a201-28ef12c9a374";
    
    public MediaDescriptorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getVideos method, of class MediaDescriptorTest.
     */
    @Test
    public void testGetVideos() {
        System.out.println("getVideos");
        MediaDescriptor instance = new MediaDescriptor(PATH, new PlayerController());
        ArrayList<Video> expResult = null;
        ArrayList<Video> result = instance.getVideos();
        assertNotSame(expResult, result);
    }

    /**
     * Test of getAudios method, of class MediaDescriptorTest.
     */
    @Test
    public void testGetAudios() {
        System.out.println("getAudios");
        MediaDescriptor instance = new MediaDescriptor(PATH, new PlayerController());
        ArrayList<Audio> expResult = null;
        ArrayList<Audio> result = instance.getAudios();
        assertNotSame(expResult, result);
    }

    /**
     * Test of getAnnotation method, of class MediaDescriptorTest.
     */
    @Test
    public void testGetAnnotation() {
        System.out.println("getAnnotation");
        MediaDescriptor instance = new MediaDescriptor(PATH, new PlayerController());
        Annotation expResult = null;
        Annotation result = instance.getAnnotation();
        assertNotSame(expResult, result);
    }

    /**
     * Test of getMaster method, of class MediaDescriptorTest.
     */
    @Test
    public void testGetMaster() {
        System.out.println("getMaster");
        MediaDescriptor instance = new MediaDescriptor(PATH, new PlayerController());
        Master expResult = null;
        Master result = instance.getMaster();
        assertNotSame(expResult, result);
    }

    /**
     * Test of saveEvent method, of class MediaDescriptorTest.
     */
    @Test
    public void testSaveEvent() {
        System.out.println("saveEvent");
        MediaDescriptor instance = new MediaDescriptor(PATH, new PlayerController());
        instance.prepareFirstProfile();
        instance.saveEvent();
    }

}
