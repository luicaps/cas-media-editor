/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.controller.MediaDescriptor;
import br.puc_rio.casmediaeditor.controller.Timeline;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * [CDT-02]
 *
 * @author Caps
 */
public class MediaPlayerCreatorTest extends Application {

    public static final String PATH = "C:\\Users\\Caps\\Documents\\CAS_events\\94c59136-3ad3-45bf-a201-28ef12c9a374";

    public MediaPlayerCreatorTest() {
        try {
            this.init();
            this.start(null);
        } catch (Exception ex) {
            Logger.getLogger(MediaPlayerCreatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @BeforeClass
    public static void setUpClass() {
        
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPlayer method, of class MediaPlayerCreator.
     */
    @Test
    public void testGetPlayer() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("getPlayer");
                int name = 0;
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                SyncPlayer snc = new DefaultSyncPlayer(desc, null, null, null, null, null, null);
                MediaPlayerCreator instance = new MediaPlayerCreator(desc, null, snc);
                MediaPlayer expResult = null;
                MediaPlayer result = instance.getPlayer(name);
                assertNotSame(expResult, result);
            }
        });
    }

    /**
     * Test of getMasterPlayer method, of class MediaPlayerCreator.
     */
    @Test
    public void testGetMasterPlayer() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("getMasterPlayer");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                SyncPlayer snc = new DefaultSyncPlayer(desc, null, null, null, null, null, null);
                MediaPlayerCreator instance = new MediaPlayerCreator(desc, null, snc);
                MediaPlayer expResult = null;
                MediaPlayer result = instance.getMasterPlayer();
                assertNotSame(expResult, result);
            }
        });
    }

    /**
     * Test of getDuration method, of class MediaPlayerCreator.
     */
    @Test
    public void testGetDuration() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("getDuration");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                SyncPlayer snc = new DefaultSyncPlayer(desc, null, null, null, null, null, null);
                MediaPlayerCreator instance = new MediaPlayerCreator(desc, null, snc);
                Duration expResult = null;
                Duration result = instance.getDuration();
                assertNotSame(expResult, result);
            }
        });
    }

    /**
     * Test of getTimeline method, of class MediaPlayerCreator.
     */
    @Test
    public void testGetTimeline() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("getTimeline");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                SyncPlayer snc = new DefaultSyncPlayer(desc, null, null, null, null, null, null);
                MediaPlayerCreator instance = new MediaPlayerCreator(desc, null, snc);
                Timeline expResult = null;
                Timeline result = instance.getTimeline();
                assertNotSame(expResult, result);
            }
        });
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    }

}
