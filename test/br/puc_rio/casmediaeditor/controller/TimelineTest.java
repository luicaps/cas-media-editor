/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc_rio.casmediaeditor.controller;

import static br.puc_rio.casmediaeditor.controller.MediaPlayerCreatorTest.PATH;
import br.puc_rio.casmediaeditor.model.Annotation;
import br.puc_rio.casmediaeditor.model.AnnotationItem;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * [CDT-03] e [CDT-04]
 *
 * @author Caps
 */
public class TimelineTest extends Application {

    public static final String PATH = "C:\\Users\\Caps\\Documents\\CAS_events\\94c59136-3ad3-45bf-a201-28ef12c9a374";

    public TimelineTest() {
        try {
            this.init();
            this.start(null);
        } catch (Exception ex) {
            Logger.getLogger(MediaPlayerCreatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addData method, of class Timeline.
     */
    @Test
    public void testAddData() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("addData");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                SyncPlayer snc = new DefaultSyncPlayer(desc, null, null, null, null, null, null);
                MediaPlayerCreator creator = new MediaPlayerCreator(desc, null, snc);
                AnnotationItem item = new AnnotationItem();
                item.setStart("000");
                item.setEnd("500");
                Timeline instance = creator.getTimeline();
                instance.addData(item);
            }
        });
    }

    /**
     * Test of changeItemStart method, of class Timeline.
     */
    @Test
    public void testChangeItemStart() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("changeItemStart");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                SyncPlayer snc = new DefaultSyncPlayer(desc, null, null, null, null, null, null);
                MediaPlayerCreator creator = new MediaPlayerCreator(desc, null, snc);
                int index = 0;
                double startMillis = 0.0;
                Timeline instance = creator.getTimeline();
                instance.changeItemStart(index, startMillis);
            }
        });
    }

    /**
     * Test of changeItemEnd method, of class Timeline.
     */
    @Test
    public void testChangeItemEnd() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("changeItemEnd");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                SyncPlayer snc = new DefaultSyncPlayer(desc, null, null, null, null, null, null);
                MediaPlayerCreator creator = new MediaPlayerCreator(desc, null, snc);
                int index = 0;
                double endMillis = 0.0;
                Timeline instance = creator.getTimeline();
                instance.changeItemEnd(index, endMillis);
            }
        });
    }

    /**
     * Test of changeItemContent method, of class Timeline.
     */
    @Test
    public void testChangeItemContent() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                System.out.println("changeItemContent");
                MediaDescriptor desc = new MediaDescriptor(PATH, new PlayerController());
                SyncPlayer snc = new DefaultSyncPlayer(desc, null, null, null, null, null, null);
                MediaPlayerCreator creator = new MediaPlayerCreator(desc, null, snc);
                int index = 0;
                String content = "";
                Timeline instance = null;
                instance.changeItemContent(index, content);
            }
        });
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
    }

}
