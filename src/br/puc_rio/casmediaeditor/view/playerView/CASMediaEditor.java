package br.puc_rio.casmediaeditor.view.playerView;

import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Application Main class. It creates the instance for the player GUI.
 *
 * @author Luiz
 */
public class CASMediaEditor extends Application {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(CASMediaEditor.class.getName());

    /**
     * Method called by JavaFX Application on start.
     *
     * @param stage The instance of the Application that receives the scenes
     * (GUI).
     * @throws Exception Throws an Exception in case of error during scene load.
     */
    @Override
    public void start(Stage stage) throws Exception {
        log.info("Starting Player window");
        Parent root = FXMLLoader.load(getClass().getResource("Player.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setX(50);
        stage.setY(50);
        stage.show();
        log.info("Player window started");
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        log.info("Application launched");
        launch(args);
    }

}
