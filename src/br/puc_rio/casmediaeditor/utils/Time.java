package br.puc_rio.casmediaeditor.utils;

import java.util.logging.Logger;
import javafx.util.Duration;

/**
 * Class to hold static methods related to time conversion from Duration formats to String presentation
 * @author Luiz
 */
public class Time {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(Time.class.getName());
    
    /**
     * Receive the time elapsed and the total time and formats it to be
     * presented on GUI
     *
     * @param elapsed time elapsed
     * @param duration current time
     * @return the string of the formatted time
     */
    public static String formatTime(Duration elapsed, Duration duration) {
        log.info("Formating time for two");
        int intElapsed = (int) Math.floor(elapsed.toSeconds());
        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = intElapsed - elapsedHours * 60 * 60 - elapsedMinutes * 60;

        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int) Math.floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60 - durationMinutes * 60;

            if (durationHours > 0) {
                return String.format("%d:%02d:%02d/%d:%02d:%02d",
                        elapsedHours, elapsedMinutes, elapsedSeconds,
                        durationHours, durationMinutes, durationSeconds);
            } else {
                return String.format("%02d:%02d/%02d:%02d",
                        elapsedMinutes, elapsedSeconds,
                        durationMinutes, durationSeconds);
            }
        } else {
            if (elapsedHours > 0) {
                return String.format("%d:%02d:%02d",
                        elapsedHours, elapsedMinutes, elapsedSeconds);
            } else {
                return String.format("%02d:%02d",
                        elapsedMinutes, elapsedSeconds);
            }
        }
    }

    /**
     * Receive the time elapsed formats it to be presented on GUI
     *
     * @param duration
     * @return String containing the text presentation for the elapsed time
     */
    public static String formatTime(Duration duration) {
        log.info("Formating time for one");
        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int) Math.floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60 - durationMinutes * 60;

            if (durationHours > 0) {
                return String.format("%d:%02d:%02d",
                        durationHours, durationMinutes, durationSeconds);
            } else {
                return String.format("%02d:%02d",
                        durationMinutes, durationSeconds);
            }
        }
        return "00:00";
    }

    /**
     * Receive the time elapsed formats it to be presented on GUI
     *
     * @param duration
     * @return String containing the text presentation for the elapsed time
     */
    public static String formatTimeMinMs(Duration duration) {
        log.info("Formating time for one with milliseconds information");
        if (duration.greaterThan(Duration.ZERO)) {
            int durationMs = (int) Math.floor(duration.toMillis());
            int durationSec = durationMs / 1000;
            int durationHours = durationSec / (60 * 60);
            if (durationHours > 0) {
                durationSec -= durationHours * 60 * 60;
            }
            durationMs -= ((durationSec * 1000) + (durationHours * 60 * 60 * 1000));
            int durationMinutes = durationSec / 60;
            int durationSeconds = durationSec - durationHours * 60 * 60 - durationMinutes * 60;

            if (durationHours > 0) {
                return String.format("%d:%02d:%02d.%02d",
                        durationHours, durationMinutes, durationSeconds, durationMs);
            } else {
                return String.format("%02d:%02d.%02d",
                        durationMinutes, durationSeconds, durationMs);
            }
        }
        return "00:00";
    }

}
