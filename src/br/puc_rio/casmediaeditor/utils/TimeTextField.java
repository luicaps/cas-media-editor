package br.puc_rio.casmediaeditor.utils;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextField;

/**
 *  TextField specialization to receive only "HH:mm:sss.SSS" and "mm:ss.SSS" formats
 * @author Luiz
 */
public class TimeTextField extends TextField {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(TimeTextField.class.getName());
    
    /**
     * Hour regex representation
     */
    private StringProperty hour = new SimpleStringProperty(this, "restrict");
    /**
     * Minute regex representation
     */
    private StringProperty minute = new SimpleStringProperty(this, "phoneMatch");

    /**
     * Default constructor
     */
    public TimeTextField() {
        log.info("Creating new instance");
        hour.set("[0-9]||"
                + "[0-9]{2}||"
                + "[0-9]{2}:||"
                + "[0-9]{2}:[0-9]||"
                + "[0-9]{2}:[0-9]{2}||"
                + "[0-9]{2}:[0-9]{2}:||"
                + "[0-9]{2}:[0-9]{2}:[0-9]||"
                + "[0-9]{2}:[0-9]{2}:[0-9]{2}||"
                + "[0-9]{2}:[0-9]{2}:[0-9]{2}.||"
                + "[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]||"
                + "[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{2}||"
                + "[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}");
        minute.set("[0-9]||"
                + "[0-9]{2}||"
                + "[0-9]{2}:||"
                + "[0-9]{2}:[0-9]||"
                + "[0-9]{2}:[0-9]{2}||"
                + "[0-9]{2}:[0-9]{2}.||"
                + "[0-9]{2}:[0-9]{2}.[0-9]||"
                + "[0-9]{2}:[0-9]{2}.[0-9]{2}||"
                + "[0-9]{2}:[0-9]{2}.[0-9]{3}");
        log.log(Level.INFO, "hour made as: {0}", hour);
        log.log(Level.INFO, "minute made as: {0}", minute);
    }

    /**
     * Changed to evaluate the new entry before accepting it
     * @param start where the change started
     * @param end where the change finished
     * @param text text changed
     */
    @Override
    public void replaceText(int start, int end, String text) {
        log.info("Replacing text");
        if ((getText() + text).matches(hour.get()) || (getText() + text).matches(minute.get())) {
            super.replaceText(start, end, text);
        } /*else {
         String[] sp = text.split(":");
         boolean ok = true;
         for (String string : sp) {
         if (!string.matches("[0-9]")) {
         ok = false;
         }
         }
         if ((sp.length <= 3) && ok) {
         super.replaceText(start, end, text);
         }
         }*/

    }

    /**
     * Changed to evaluate the new selection entry before accepting it
     * @param text New entry
     */
    @Override
    public void replaceSelection(String text) {
        log.info("replacing selection");
        if ((getText() + text).matches(hour.get()) || (getText() + text).matches(minute.get())) {
            super.replaceSelection(text);
        } /*else {
         String[] sp = text.split(":");
         boolean ok = true;
         for (String string : sp) {
         if (!string.matches("[0-9]")) {
         ok = false;
         }
         }
         if ((sp.length <= 3) && ok) {
         super.replaceSelection(text);
         }
         }*/

    }

}
