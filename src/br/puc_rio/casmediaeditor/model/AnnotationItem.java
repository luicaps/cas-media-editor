package br.puc_rio.casmediaeditor.model;

import br.puc_rio.casmediaeditor.utils.Time;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.Duration;

/**
 * Model class that represents a single annotation from the
 * {@link Annotation#annotations} set
 *
 * @author Luiz
 */
public class AnnotationItem implements Comparable<AnnotationItem> {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(AnnotationItem.class.getName());

    /**
     * The className used for the timeline. With this name, it changes de CSS
     * used for this annotation, to differentiate from the others
     */
    private String className;
    /**
     * The annotation information to be presented
     */
    private StringProperty content;
    /**
     * The start time in milliseconds, which is related to the event time
     */
    private DoubleProperty startMillis;
    /**
     * The finish time in milliseconds, which is related to the event time
     */
    private DoubleProperty endMillis;
    /**
     * Formatted start time presented in "HH:mm:ss.SSS" or "mm:ss.SSS" format
     */
    private StringProperty start;
    /**
     * Formatted finish time presented in "HH:mm:ss.SSS" or "mm:ss.SSS" format
     */
    private StringProperty end;
    /**
     * The unique id of the annotation
     */
    private int id;

    /**
     * Only constructor used to create all instances
     */
    public AnnotationItem() {
        log.info("Creating new AnnotationItem");
        content = new SimpleStringProperty();
        startMillis = new SimpleDoubleProperty();
        endMillis = new SimpleDoubleProperty();
        start = new SimpleStringProperty();
        end = new SimpleStringProperty();
        className = "";
    }

    /**
     * Getter for {@link AnnotationItem#className}
     *
     * @return the {@link AnnotationItem#className} value
     */
    public String getClassName() {
        log.log(Level.INFO, "Getting class from {0}", id);
        return className;
    }

    /**
     * Setter for {@link AnnotationItem#className}
     *
     * @param className the new value for {@link AnnotationItem#className}
     */
    public void setClassName(String className) {
        log.log(Level.INFO, "Setting class as {0} for {1}", new Object[]{className, id});
        this.className = className;
    }

    /**
     * Getter for {@link AnnotationItem#content} property value
     *
     * @return the {@link AnnotationItem#content} property value
     */
    public String getContent() {
        log.info("Getting content for " + id);
        return content.getValue();
    }

    /**
     * Setter for {@link AnnotationItem#content} property value
     *
     * @param content new value for {@link AnnotationItem#content} property
     * value
     */
    public void setContent(String content) {
        log.log(Level.INFO, "Setting content as {0} for {1}", new Object[]{content, id});
        this.content.setValue(content);
    }

    /**
     * Retrieve the property object of {@link AnnotationItem#content}
     *
     * @return the {@link AnnotationItem#content} object
     */
    public StringProperty contentProperty() {
        log.log(Level.INFO, "Accessing contentProperty for {0}", id);
        return content;
    }

    /**
     * Getter for {@link AnnotationItem#start} property value
     *
     * @return the {@link AnnotationItem#start} property value
     */
    public String getStart() {
        log.log(Level.INFO, "Getting start String value for {0}", id);
        return start.getValue();
    }

    /**
     * Setter for {@link AnnotationItem#start} property value
     *
     * @param start the new {@link AnnotationItem#start} property value
     */
    public void setStart(String start) {
        log.log(Level.INFO, "Setting start String as {0} for {1}", new Object[]{start, id});
        setStartMillis(timeToDouble(start));
        this.start.setValue(start);
    }

    /**
     * Retrieve the property object of {@link AnnotationItem#start}
     *
     * @return the {@link AnnotationItem#start} object
     */
    public StringProperty startProperty() {
        log.log(Level.INFO, "Accessing startProperty for {0}", id);
        return start;
    }

    /**
     * Getter for {@link AnnotationItem#end} property value
     *
     * @return the {@link AnnotationItem#end} property value
     */
    public String getEnd() {
        log.log(Level.INFO, "Getting end String value for {0}", id);
        return end.getValue();
    }

    /**
     * Setter for {@link AnnotationItem#end} property value
     *
     * @param end the new {@link AnnotationItem#end} property value
     */
    public void setEnd(String end) {
        log.log(Level.INFO, "Setting end String as {0} for {1}", new Object[]{end, id});
        setEndMillis(timeToDouble(end));
        this.end.setValue(end);
    }

    /**
     * Retrieve the property object of {@link AnnotationItem#end}
     *
     * @return the {@link AnnotationItem#end} object
     */
    public StringProperty endProperty() {
        log.log(Level.INFO, "Accessing endProperty for {0}", id);
        return end;
    }

    /**
     * Getter for {@link AnnotationItem#startMillis} property value
     *
     * @return the {@link AnnotationItem#startMillis} property value
     */
    public double getStartMillis() {
        log.log(Level.INFO, "Getting start double for {0}", id);
        return startMillis.getValue();
    }

    /**
     * Setter for {@link AnnotationItem#startMillis} property value
     *
     * @param startMillis the new {@link AnnotationItem#startMillis} property
     * value
     */
    public void setStartMillis(double startMillis) {
        log.log(Level.INFO, "Setting start double as {0} for {1}", new Object[]{startMillis, id});
        start.setValue(Time.formatTimeMinMs(new Duration(startMillis)));
        this.startMillis.setValue(startMillis);
    }

    /**
     * Retrieve the property object of {@link AnnotationItem#startMillis}
     *
     * @return the {@link AnnotationItem#startMillis} object
     */
    public DoubleProperty startMillisProperty() {
        log.log(Level.INFO, "Accessing startMillisProperty for {0}", id);
        return startMillis;
    }

    /**
     * Getter for {@link AnnotationItem#endMillis} property value
     *
     * @return the {@link AnnotationItem#endMillis} property value
     */
    public double getEndMillis() {
        log.log(Level.INFO, "Getting end double for {0}", id);
        return endMillis.getValue();
    }

    /**
     * Setter for {@link AnnotationItem#endMillis} property value
     *
     * @param endMillis the new {@link AnnotationItem#endMillis} property value
     */
    public void setEndMillis(double endMillis) {
        log.log(Level.INFO, "Setting end double as {0} for {1}", new Object[]{endMillis, id});
        end.setValue(Time.formatTimeMinMs(new Duration(endMillis)));
        this.endMillis.setValue(endMillis);
    }

    public DoubleProperty endMillisProperty() {
        log.log(Level.INFO, "Accessing endMillisProperty for {0}", id);
        return endMillis;
    }

    /**
     * Override for compareTo to sort the Annotation set compare to the starting
     * time
     *
     * @param item item to be compared to the instance
     * @return positive if the instance is greater
     */
    @Override
    public int compareTo(AnnotationItem item) {
        log.log(Level.INFO, "Comparing {0} with {1}", new Object[]{id, item.getId()});
        double s = ((AnnotationItem) item).getStartMillis();
        return (int) (getStartMillis() - s);
    }

    /**
     * Getter for id
     *
     * @return the id value
     */
    public int getId() {
        log.info("Getting id");
        return id;
    }

    /**
     * Setter for id
     *
     * @param id the new id value
     */
    public void setId(int id) {
        log.info("Setting id");
        this.id = id;
    }

    /**
     * Converts a String value for time in the format "HH:mm:ss.SSS" or
     * "mm:ss.SSS" to a double in milliseconds
     *
     * @param time the String value for the time to be converted
     * @return the double value in milliseconds
     */
    private double timeToDouble(String time) {
        log.log(Level.INFO, "Converting time {0}", time);
        String[] sp = time.split(":");
        int result = 0;
        if (sp.length == 2) {
            int min = Integer.parseInt(sp[0]);
            String[] spl = sp[1].split("\\.");
            int sec = Integer.parseInt(spl[0]);
            int millis = Integer.parseInt(spl[1]);
            result = (min * 60 * 1000) + (sec * 1000) + millis;
        } else {
            int hour = Integer.parseInt(sp[0]);
            int min = Integer.parseInt(sp[1]);
            String[] spl = sp[2].split("\\.");
            int sec = Integer.parseInt(spl[0]);
            int millis = Integer.parseInt(spl[1]);
            result = (hour * 60 * 60 * 1000) + (min * 60 * 1000) + (sec * 1000) + millis;
        }
        return result;
    }

}
