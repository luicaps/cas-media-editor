package br.puc_rio.casmediaeditor.model;

import br.puc_rio.casmediaeditor.model.AnnotationItem;
import java.util.Date;

/**
 * Class that represents the {@link AnnotationItem} class as a JSON object. This
 * an exact copy of the {@link AnnotationItem} class, refer to it to know its
 * purposes. This class is used so no unnecessary information is sent to the
 * timeline, and also to guarantee that the necessary information is sent
 *
 * @author Luiz
 */
public class SubtitleJson {

    /**
     * Start time of the annotation
     */
    private Date start;
    /**
     * End time of the annotation
     */
    private Date end;
    /**
     * Content of the annotation
     */
    private String content;
    /**
     * className of the annotation
     */
    private String className;
    /**
     * unique id of the annotation
     */
    private int id;

    /**
     * Custom constructor
     *
     * @param start start date of the text
     * @param end end date of the text
     * @param content content of the text
     * @param className the className to be attributed as css class
     * @param id id of the text
     */
    public SubtitleJson(Date start, Date end, String content, String className, int id) {
        this.start = start;
        this.end = end;
        this.content = content;
        this.className = className;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
