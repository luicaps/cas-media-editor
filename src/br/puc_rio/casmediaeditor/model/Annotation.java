package br.puc_rio.casmediaeditor.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Model class that represents the set of annotations from a annotation media
 *
 * @author Luiz
 */
public class Annotation {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(Annotation.class.getName());

    /**
     * Unique media ID to identify this media during the execution
     */
    private int id;
    /**
     * The ID for the media in the NCL eventDescription
     */
    private String idName;
    /**
     * The set of annotations in the srt media file
     */
    private ArrayList<AnnotationItem> annotations;
    /**
     * The start date property defined in the eventDescription
     */
    private Date startDate;
    /**
     * The finish date property defined in the eventDescription
     */
    private Date finishDate;
    /**
     * The same ID as {@link Annotation#idName}. This is used because the user
     * can change this name, but the original ID needs to be kept so it can be
     * changed in the description files
     */
    private String originalId;

    /**
     * Getter for {@link Annotation#originalId}
     *
     * @return the {@link Annotation#originalId} value
     */
    public String getOriginalId() {
        log.info("Getting originalId value");
        return originalId;
    }

    /**
     * Setter for {@link Annotation#originalId}
     *
     * @param originalId New value for {@link Annotation#originalId}
     */
    public void setOriginalId(String originalId) {
        log.info("Setting originalId value");
        this.originalId = originalId;
    }

    /**
     * Getter for {@link Annotation#id}
     *
     * @return the {@link Annotation#id} value
     */
    public int getId() {
        log.info("Getting Id value");
        return id;
    }

    /**
     * Setter for {@link Annotation#id}
     *
     * @param id new value for {@link Annotation#id}
     */
    public void setId(int id) {
        log.info("Setting Id value");
        this.id = id;
    }

    /**
     * Getter for {@link Annotation#idName}
     *
     * @return the {@link Annotation#idName} value
     */
    public String getIdName() {
        log.log(Level.INFO, "Getting IdName value for {0}", id);
        return idName;
    }

    /**
     * Setter for {@link Annotation#idName}
     *
     * @param idName new value for {@link Annotation#idName}
     */
    public void setIdName(String idName) {
        log.log(Level.INFO, "Setting IdName value as {0} for {1}", new Object[]{idName, id});
        this.idName = idName;
    }

    /**
     * Shifts all annotation's start and end times. This is used to adjust the
     * times based on the master media, described in the eventDescription
     *
     * @param millis time to shift the annotation set
     */
    public void adjustAnnotations(double millis) {
        log.log(Level.INFO, "Adjusting annotations in {0} milliseconds", millis);
        for (AnnotationItem annotationItem : annotations) {
            annotationItem.setStartMillis(annotationItem.getStartMillis() + millis);
            annotationItem.setEndMillis(annotationItem.getEndMillis() + millis);
        }
        log.info("Adjusted");
    }

    /**
     * Get the annotation from the annotation set using his unique id
     *
     * @param id the unique id of the annotation
     * @return the annotation that have the id. Return null if no annotation is
     * found
     */
    public AnnotationItem getAnnotationById(int id) {
        log.log(Level.INFO, "Searching annotation for id: {0}", id);
        for (AnnotationItem annotationItem : annotations) {
            if (annotationItem.getId() == id) {
                log.info("Annotation found");
                return annotationItem;
            }
        }
        log.info("Annotation NOT found");
        return null;
    }

    /**
     * Getter for {@link Annotation#annotations}
     *
     * @return the {@link Annotation#annotations} value
     */
    public ArrayList<AnnotationItem> getAnnotations() {
        log.info("Getting annotation set");
        return annotations;
    }

    /**
     * Setter for {@link Annotation#annotations}
     *
     * @param annotations set of annotations to be set
     */
    public void setAnnotations(ArrayList<AnnotationItem> annotations) {
        log.info("Setting annotation set");
        this.annotations = annotations;
    }

    /**
     * Getter for {@link Annotation#startDate}
     *
     * @return the {@link Annotation#startDate} value
     */
    public Date getStartDate() {
        log.log(Level.INFO, "Getting start date for {0}", id);
        return startDate;
    }

    /**
     * Setter for {@link Annotation#startDate}
     *
     * @param startDate new value for {@link Annotation#startDate}
     */
    public void setStartDate(Date startDate) {
        log.log(Level.INFO, "Setting start date as {0} for {1}", new Object[]{startDate, id});
        this.startDate = startDate;
    }

    /**
     * Getter for {@link Annotation#finishDate}
     *
     * @return the {@link Annotation#finishDate} value
     */
    public Date getFinishDate() {
        log.log(Level.INFO, "Getting finish date for {0}", id);
        return finishDate;
    }

    /**
     * Setter for {@link Annotation#finishDate}
     *
     * @param finishDate new value for {@link Annotation#finishDate}
     */
    public void setFinishDate(Date finishDate) {
        log.log(Level.INFO, "Setting finish date as {0} for {1}", new Object[]{finishDate, id});
        this.finishDate = finishDate;
    }

}
