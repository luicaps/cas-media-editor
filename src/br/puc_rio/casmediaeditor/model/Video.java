package br.puc_rio.casmediaeditor.model;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Model class that represents an Video media in the event
 *
 * @author Luiz
 */
public class Video {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(Video.class.getName());

    /**
     * unique id of the audio
     */
    private int id;
    /**
     * The ID for the media in the NCL eventDescription
     */
    private String idName;
    /**
     * the src path for the original audio file in the file system
     */
    private String src;
    /**
     * The start date defined in the eventDescription
     */
    private Date startDate;
    /**
     * The finish date defined in the eventDescription
     */
    private Date finishDate;
    /**
     * The same ID as {@link Video#idName}. This is used because the user can
     * change this name, but the original ID needs to be kept so it can be
     * changed in the description files
     */
    private String originalId;

    /**
     * Default constructor
     *
     * @param idName value for {@link Video#idName}
     * @param src value for {@link Video#src}
     * @param startDate value for {@link Video#startDate}
     * @param finishDate value for {@link Video#finishDate}
     */
    public Video(String idName, String src, Date startDate, Date finishDate) {
        log.info("Creating a Video instance");
        this.idName = idName;
        this.originalId = idName;
        this.src = src;
        this.startDate = startDate;
        this.finishDate = finishDate;
        log.log(Level.INFO, "Complete: {0}", this.toString());
    }

    /**
     * Getter for {@link Video#originalId}
     *
     * @return the {@link Video#originalId} value
     */
    public String getOriginalId() {
        log.info("Getting originalId value");
        return originalId;
    }

    /**
     * Getter for {@link Video#id}
     *
     * @return the {@link Video#id} value
     */
    public int getId() {
        log.info("Getting Id value");
        return id;
    }

    /**
     * Setter for {@link Video#id}
     *
     * @param id new value for {@link Video#id}
     */
    public void setId(int id) {
        log.info("Setting Id value");
        this.id = id;
    }

    /**
     * Getter for {@link Video#idName}
     *
     * @return the {@link Video#idName} value
     */
    public String getIdName() {
        log.log(Level.INFO, "Getting IdName value for {0}", id);
        return idName;
    }

    /**
     * Setter for {@link Video#idName}
     *
     * @param idName new value for {@link Video#idName}
     */
    public void setIdName(String idName) {
        log.log(Level.INFO, "Setting IdName value as {0} for {1}", new Object[]{idName, id});
        this.idName = idName;
    }

    /**
     * Getter for {@link Video#src}
     *
     * @return the {@link Video#src} value
     */
    public String getSrc() {
        log.log(Level.INFO, "Getting src for {0}", id);
        return src;
    }

    /**
     * Setter for {@link Video#src}
     *
     * @param src the new value for {@link Video#src}
     */
    public void setSrc(String src) {
        log.log(Level.INFO, "Setting src as {0} for {1}", new Object[]{src, id});
        this.src = src;
    }

    /**
     * Getter for {@link Video#startDate}
     *
     * @return the {@link Video#startDate} value
     */
    public Date getStartDate() {
        log.log(Level.INFO, "Getting start date for {0}", id);
        return startDate;
    }

    /**
     * Setter for {@link Video#startDate}
     *
     * @param startDate new value for {@link Video#startDate}
     */
    public void setStartDate(Date startDate) {
        log.log(Level.INFO, "Setting start date as {0} for {1}", new Object[]{startDate, id});
        this.startDate = startDate;
    }

    /**
     * Getter for {@link Video#finishDate}
     *
     * @return the {@link Video#finishDate} value
     */
    public Date getFinishDate() {
        log.log(Level.INFO, "Getting finish date for {0}", id);
        return finishDate;
    }

    /**
     * Setter for {@link Video#finishDate}
     *
     * @param finishDate new value for {@link Video#finishDate}
     */
    public void setFinishDate(Date finishDate) {
        log.log(Level.INFO, "Setting finish date as {0} for {1}", new Object[]{finishDate, id});
        this.finishDate = finishDate;
    }

}
