package br.puc_rio.casmediaeditor.model;

import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Model class that represents the master media in the event. This media is
 * unique, which means there's only one instance of this class for each event
 *
 * @author Luiz
 */
public class Master {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(Master.class.getName());

    /**
     * ID name for the event
     */
    private String eventId;
    /**
     * Explicit duration of the event in milliseconds
     */
    private double explicitDur;
    /**
     * The start date defined in the eventDescription
     */
    private Date startDate;
    /**
     * The finish date defined in the eventDescription
     */
    private Date finishDate;
    /**
     * Areas defined for master media in the eventDescription
     */
    private HashMap<Integer, Area> areas;
    /**
     * the src path for the original audio file in the file system
     */
    private String src;

    /**
     * Default constructor
     *
     * @param eventId value for {@link Master#eventId}
     * @param explicitDur value for {@link Master#explicitDur}
     * @param startDate value for {@link Master#startDate}
     * @param finishDate value for {@link Master#finishDate}
     * @param src value for {@link Master#src}
     */
    public Master(String eventId, double explicitDur, Date startDate, Date finishDate, String src) {
        log.info("Creating Master instance");
        this.eventId = eventId;
        this.explicitDur = explicitDur;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.src = src;
        areas = new HashMap();
        log.log(Level.INFO, "Complete: {0}", this.toString());
    }

    /**
     * Getter for {@link Master#src}
     *
     * @return the {@link Master#src} value
     */
    public String getSrc() {
        log.log(Level.INFO, "Getting src for {0}", eventId);
        return src;
    }

    /**
     * Adds a new area to the master media
     *
     * @param id unique id of the area
     * @param idName id of the area in the NCL eventDescription
     * @param begin the start time of the area
     * @param end the end time of the area
     */
    public void insertArea(int id, String idName, double begin, double end) {
        log.log(Level.INFO, "InsertId: {0}, content: {1},begin: {2}, end: {3}", new Object[]{id, idName, begin, end});
        areas.put(id, new Area(idName, begin, end));
    }

    /**
     * Get the start (begin) time of the area linked to the given id
     *
     * @param id the unique id of the area
     * @return the start time of the area linked to the id
     */
    public double getAreaStartTime(int id) {
        log.log(Level.INFO, "getAreaStartFor: {0}, start: {1}", new Object[]{id, areas.get(id).begin});
        return areas.get(id).begin;
    }

    /**
     * Get the end time of the area linked to the given id
     *
     * @param id the unique id of the area
     * @return the end time of the area linked to the id
     */
    public double getAreaEndTime(int id) {
        log.log(Level.INFO, "getAreaEndFor: {0}, end: {1}", new Object[]{id, areas.get(id).end});
        return areas.get(id).end;
    }

    /**
     * Getter for {@link Master#eventId}
     *
     * @return the {@link Master#eventId} value
     */
    public String getEventId() {
        log.info("Getting event id");
        return eventId;
    }

    /**
     * Setter for {@link Master#eventId}
     *
     * @param eventId the new value for {@link Master#eventId}
     */
    public void setEventId(String eventId) {
        log.info("Setting event id");
        this.eventId = eventId;
    }

    /**
     * Getter for {@link Master#explicitDur}
     *
     * @return the {@link Master#explicitDur} value
     */
    public double getExplicitDur() {
        log.log(Level.INFO, "Getting explicit duration for {0}", eventId);
        return explicitDur;
    }

    /**
     * Setter for {@link Master#explicitDur}
     *
     * @param explicitDur the new value for {@link Master#explicitDur}
     */
    public void setExplicitDur(double explicitDur) {
        this.explicitDur = explicitDur;
    }

    /**
     * Getter for {@link Master#startDate}
     *
     * @return the {@link Master#startDate} value
     */
    public Date getStartDate() {
        log.log(Level.INFO, "Getting start date for {0}", eventId);
        return startDate;
    }

    /**
     * Setter for {@link Master#startDate}
     *
     * @param startDate new value for {@link Master#startDate}
     */
    public void setStartDate(Date startDate) {
        log.log(Level.INFO, "Setting start date as {0} for {1}", new Object[]{startDate, eventId});
        this.startDate = startDate;
    }

    /**
     * Getter for {@link Master#finishDate}
     *
     * @return the {@link Master#finishDate} value
     */
    public Date getFinishDate() {
        log.log(Level.INFO, "Getting finish date for {0}", eventId);
        return finishDate;
    }

    /**
     * Setter for {@link Master#finishDate}
     *
     * @param finishDate new value for {@link Master#finishDate}
     */
    public void setFinishDate(Date finishDate) {
        log.log(Level.INFO, "Setting finish date as {0} for {1}", new Object[]{finishDate, eventId});
        this.finishDate = finishDate;
    }

    /**
     * Defines the model for an Area in the master media NCL eventDescription
     */
    public class Area {

        /**
         * The id of the area
         */
        private String idName;
        /**
         * Start time of the area
         */
        private double begin;
        /**
         * end time of the area
         */
        private double end;

        /**
         * Default constructor
         *
         * @param idName value for {@link Master.Area#idName}
         * @param begin value for {@link Master.Area#begin}
         * @param end value for {@link Master.Area#end}
         */
        public Area(String idName, double begin, double end) {
            this.idName = idName;
            this.begin = begin;
            this.end = end;
        }

        /**
         * Getter for {@link Master.Area#begin}
         *
         * @return the {@link Master.Area#begin} value
         */
        public double getBegin() {
            return begin;
        }

        /**
         * Setter for {@link Master.Area#begin}
         *
         * @param begin the new value for {@link Master.Area#begin}
         */
        public void setBegin(double begin) {
            this.begin = begin;
        }

        /**
         * Getter for {@link Master.Area#end}
         *
         * @return the {@link Master.Area#end} value
         */
        public double getEnd() {
            return end;
        }

        /**
         * Setter for {@link Master.Area#end}
         *
         * @param end the new value for {@link Master.Area#end}
         */
        public void setEnd(double end) {
            this.end = end;
        }

    }

}
