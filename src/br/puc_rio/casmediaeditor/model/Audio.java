package br.puc_rio.casmediaeditor.model;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Model class that represents an Audio media in the event
 *
 * @author Luiz
 */
public class Audio {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(Audio.class.getName());

    /**
     * unique id of the audio
     */
    private int id;
    /**
     * The ID for the media in the NCL eventDescription
     */
    private String idName;
    /**
     * the src path for the original audio file in the file system
     */
    private String src;
    /**
     * The start date defined in the eventDescription
     */
    private Date startDate;
    /**
     * The finish date defined in the eventDescription
     */
    private Date finishDate;
    /**
     * The gap between the start of the media described in its tag in
     * eventDescription and the start time for this media described in the area
     * for the master media. This is used for synchronization purposes
     */
    private double timeGap = 0;
    /**
     * The same ID as {@link Audio#idName}. This is used because the user can
     * change this name, but the original ID needs to be kept so it can be
     * changed in the description files
     */
    private String originalId;

    /**
     * Default constructor
     *
     * @param idName value for {@link Audio#idName}
     * @param src value for {@link Audio#src}
     * @param startDate value for {@link Audio#startDate}
     * @param finishDate value for {@link Audio#finishDate}
     */
    public Audio(String idName, String src, Date startDate, Date finishDate) {
        log.info("Creating an Audio instance");
        this.idName = idName;
        this.originalId = idName;
        this.src = src;
        this.startDate = startDate;
        this.finishDate = finishDate;
        log.log(Level.INFO, "Complete: {0}", this.toString());
    }

    /**
     * Getter for {@link Audio#originalId}
     *
     * @return the {@link Audio#originalId} value
     */
    public String getOriginalId() {
        log.info("Getting originalId value");
        return originalId;
    }

    /**
     * Getter for {@link Audio#id}
     *
     * @return the {@link Audio#id} value
     */
    public int getId() {
        log.info("Getting Id value");
        return id;
    }

    /**
     * Setter for {@link Audio#id}
     *
     * @param id new value for {@link Audio#id}
     */
    public void setId(int id) {
        log.info("Setting Id value");
        this.id = id;
    }

    /**
     * Getter for {@link Audio#idName}
     *
     * @return the {@link Audio#idName} value
     */
    public String getIdName() {
        log.log(Level.INFO, "Getting IdName value for {0}", id);
        return idName;
    }

    /**
     * Setter for {@link Audio#idName}
     *
     * @param idName new value for {@link Audio#idName}
     */
    public void setIdName(String idName) {
        log.log(Level.INFO, "Setting IdName value as {0} for {1}", new Object[]{idName, id});
        this.idName = idName;
    }

    /**
     * Getter for {@link Audio#src}
     *
     * @return the {@link Audio#src} value
     */
    public String getSrc() {
        log.log(Level.INFO, "Getting src for {0}", id);
        return src;
    }

    /**
     * Setter for {@link Audio#src}
     *
     * @param src the new value for {@link Audio#src}
     */
    public void setSrc(String src) {
        log.log(Level.INFO, "Setting src as {0} for {1}", new Object[]{src, id});
        this.src = src;
    }

    /**
     * Getter for {@link Audio#startDate}
     *
     * @return the {@link Audio#startDate} value
     */
    public Date getStartDate() {
        log.log(Level.INFO, "Getting start date for {0}", id);
        return startDate;
    }

    /**
     * Setter for {@link Audio#startDate}
     *
     * @param startDate new value for {@link Audio#startDate}
     */
    public void setStartDate(Date startDate) {
        log.log(Level.INFO, "Setting start date as {0} for {1}", new Object[]{startDate, id});
        this.startDate = startDate;
    }

    /**
     * Getter for {@link Audio#finishDate}
     *
     * @return the {@link Audio#finishDate} value
     */
    public Date getFinishDate() {
        log.log(Level.INFO, "Getting finish date for {0}", id);
        return finishDate;
    }

    /**
     * Setter for {@link Audio#finishDate}
     *
     * @param finishDate new value for {@link Audio#finishDate}
     */
    public void setFinishDate(Date finishDate) {
        log.log(Level.INFO, "Setting finish date as {0} for {1}", new Object[]{finishDate, id});
        this.finishDate = finishDate;
    }

    /**
     * Getter for {@link Audio#timeGap}
     *
     * @return the {@link Audio#timeGap} value
     */
    public double getTimeGap() {
        log.log(Level.INFO, "Getting time gap for {0}", id);
        return timeGap;
    }

    /**
     * Setter for {@link Audio#timeGap}
     *
     * @param timeGap new value for {@link Audio#timeGap}
     */
    public void setTimeGap(double timeGap) {
        log.log(Level.INFO, "Setting time gap as {0} for {1}", new Object[]{timeGap, id});
        this.timeGap = timeGap;
    }

}
