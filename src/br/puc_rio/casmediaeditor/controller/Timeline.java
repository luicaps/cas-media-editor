package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.model.SubtitleJson;
import br.puc_rio.casmediaeditor.model.Annotation;
import br.puc_rio.casmediaeditor.model.AnnotationItem;
import br.puc_rio.casmediaeditor.controller.SyncPlayer;
import br.puc_rio.casmediaeditor.controller.PlayerController;
import br.puc_rio.casmediaeditor.controller.TableViewController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Duration;
import netscape.javascript.JSObject;

/**
 * Timeline class controller. It creates and controls the timeline instance
 *
 * @author Luiz
 */
public class Timeline {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(Timeline.class.getName());

    /**
     * WebView that shows the timeline
     */
    WebView view;
    /**
     * WebEngine that controls the timeline interaction
     */
    WebEngine eng;
    /**
     * Flag to adjust interface after it's ready
     */
    boolean isready = false;
    /**
     * Total duration information
     */
    Duration duration;
    /**
     * Name of the file of the timeline page to be opened
     */
    private static final String fileName = "/br/puc_rio/casmediaeditor/view/timeline/timeline.html";

    public Timeline(final Pane parent, final Annotation annotations, Duration duration, final SyncPlayer player) {
        log.info("Creating Timeline instance");
        this.duration = duration;
        view = new WebView();
        view.setMinSize(500, 400);
        view.setPrefSize(500, 400);
        eng = view.getEngine();
        log.info("WebView and Engine created");
        //HTML File opening
        URL path = getClass().getResource(fileName);
        log.log(Level.INFO, "Path for HTML found at: {0}", path.toString());
        eng.getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
            @Override
            public void changed(ObservableValue<? extends Worker.State> ov, Worker.State t, Worker.State t1) {
                if (t1 == Worker.State.SUCCEEDED) {
                    isready = true;
                    JSObject jso = (JSObject) eng.executeScript("window");
                    jso.setMember("player", player);
                    jso.setMember("logger", new JSLog());
                    eng.executeScript("drawVisualization(" + parseAnnotations(annotations) + "," + getDuration() + ");");
                    view.setPrefWidth(parent.getWidth());
                    if (isready) {
                        adjustHeight();
                    }
                }
            }
        });
        log.info("Loading page");
        eng.load(path.toString());
        parent.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                view.setPrefWidth(t1.doubleValue());
                if (isready) {
                    adjustHeight();
                }
            }
        });
        parent.getChildren().add(view);
        log.info("Timeline created");
    }

    /**
     * Glitch correction to adjust the page height according to the JavaFX GUI
     * height
     */
    private void adjustHeight() {
        log.info("Adjusting page height");
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                Object result = eng.executeScript("document.getElementById('timeline').offsetHeight");
                if (result instanceof Integer) {
                    Integer i = (Integer) result;
                    double height = new Double(i);
                    height = height + 20;
                    view.setPrefHeight(height);
                }
            }
        });
    }

    /**
     * Getter for {@link  Timeline#view}
     *
     * @return the {@link  Timeline#view}
     */
    public WebView getView() {
        return view;
    }

    /**
     * Seek call for the timeline.
     *
     * @param milisec The current time for seek
     */
    public void animateTo(int milisec) {
        log.info("Seeking in the timeline");
        if (isready) {
            eng.executeScript("animateTo(new Date(2013, 2, 2, 0,0,0," + milisec + "));");
        }
    }

    /**
     * Call to add new AnnotationItem generated from the user in the
     * {@link TableViewController} instance
     *
     * @param item
     */
    public void addData(AnnotationItem item) {
        log.info("Adding new data to the timeline");
        try {
            DateFormat formater = new SimpleDateFormat("yyyy,MM,dd,HH,mm,ss,SS");
            SubtitleJson sub = new SubtitleJson(formater.parse("2013,2,2,0,0,0," + Double.toString(item.getStartMillis())), formater.parse("2013,2,2,0,0,0," + Double.toString(item.getEndMillis())), item.getContent(), item.getClassName(), item.getId());
            Gson gson = new GsonBuilder().setDateFormat("yyyy,MM,dd,HH,mm,ss,SS").create();
            eng.executeScript("addData(" + gson.toJson(sub) + ");");
        } catch (ParseException ex) {
            Logger.getLogger(Timeline.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Change start call generated from the user in the
     * {@link TableViewController} instance
     *
     * @param index Unique id of the annotation
     * @param startMillis new value to be changed
     */
    public void changeItemStart(int index, double startMillis) {
        log.log(Level.INFO, "Changing item {0}", index);
        try {
            DateFormat formater = new SimpleDateFormat("yyyy,MM,dd,HH,mm,ss,SS");
            Gson gson = new GsonBuilder().setDateFormat("yyyy,MM,dd,HH,mm,ss,SS").create();
            Date s = formater.parse("2013,2,2,0,0,0," + Double.toString(startMillis));
            String o = gson.toJson(s);
            eng.executeScript("changeEventStart(" + index + ", " + o + ");");
        } catch (ParseException ex) {
            Logger.getLogger(Timeline.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Change end call generated from the user in the
     * {@link TableViewController} instance
     *
     * @param index Unique id of the annotation
     * @param endMillis new value to be changed
     */
    public void changeItemEnd(int index, double endMillis) {
        try {
            DateFormat formater = new SimpleDateFormat("yyyy,MM,dd,HH,mm,ss,SS");
            Gson gson = new GsonBuilder().setDateFormat("yyyy,MM,dd,HH,mm,ss,SS").create();
            Date s = formater.parse("2013,2,2,0,0,0," + Double.toString(endMillis));
            String o = gson.toJson(s);
            eng.executeScript("changeEventEnd(" + index + ", " + o + ");");
        } catch (ParseException ex) {
            Logger.getLogger(Timeline.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Change content call generated from the user in the
     * {@link TableViewController} instance
     *
     * @param index Unique id of the annotation
     * @param content new value to be changed
     */
    public void changeItemContent(int index, String content) {
        eng.executeScript("changeEventContent(" + index + ", '" + content + "');");
    }

    /**
     * Change selection call generated from the user in the
     * {@link TableViewController} instance
     *
     * @param index Unique id of the annotation
     */
    public void changeSelection(int index) {
        eng.executeScript("timeline.setSelection([{row: " + index + "}]);");
    }

    /**
     * Parse call that converts all {@link AnnotationItem} to
     * {@link SubtitleJson} instances
     *
     * @param annotations current {@link Annotation} instance
     * @return The JSON String of the Annotation set
     */
    public String parseAnnotations(Annotation annotations) {
        log.info("Parsing Annotations");
        ArrayList<SubtitleJson> list = new ArrayList();
        try {
            DateFormat formater = new SimpleDateFormat("yyyy,MM,dd,HH,mm,ss,SS");
            for (AnnotationItem an : annotations.getAnnotations()) {
                list.add(new SubtitleJson(formater.parse("2013,2,2,0,0,0," + Double.toString(an.getStartMillis())), formater.parse("2013,2,2,0,0,0," + Double.toString(an.getEndMillis())), an.getContent(), an.getClassName(), an.getId()));
            }
        } catch (ParseException ex) {
            Logger.getLogger(PlayerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Gson gson = new GsonBuilder().setDateFormat("yyyy,MM,dd,HH,mm,ss,SS").create();
        String ret = gson.toJson(list.toArray());
        log.log(Level.INFO, "Parse Complete: {0}", ret);
        return ret;
    }

    /**
     * Getter for the total duration
     *
     * @return the {@link Timeline#duration} value in seconds
     */
    public final int getDuration() {
        return (int) (Math.floor(duration.toSeconds()) + 1);
    }

    /**
     * Callback from the WebEngine to send log information
     */
    public class JSLog {

        private final Logger log = Logger.getLogger(JSLog.class.getName());

        public void log(String log) {
            this.log.info(log);
        }
    }

}
