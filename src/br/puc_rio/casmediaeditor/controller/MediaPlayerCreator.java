package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.model.Audio;
import br.puc_rio.casmediaeditor.model.Master;
import br.puc_rio.casmediaeditor.model.Video;
import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 * This class is intended to hold the instance for the MediaPlayer for all media
 * in the event, even though not all might be used. Yet this implementation
 * proves to be stable, but if too much memory is used, then this class should
 * be changed to open only the media needed.
 *
 * @author Luiz
 */
public class MediaPlayerCreator {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(MediaPlayerCreator.class.getName());

    /**
     * MediaDescriptor is the class with the metadada of all media
     */
    private MediaDescriptor descriptor;
    /**
     * Map with the MediaPlayer for all playable media
     */
    private HashMap<Integer, MediaPlayer> players;
    /**
     * Master is the class that has the control of the event, all media
     * synchronization are made based on this media, which is an audio file with
     * time enough to hold all media duration.
     */
    private Master master;
    /**
     * The MediaPlayer for the master media
     */
    private MediaPlayer masterPlayer;
    /**
     * The timeline of the media an annotations to be shown in the scene
     */
    private Timeline timeline;

    /**
     * Default constructor that creates all media instances, including the
     * timeline.
     *
     * @param descriptor MediaDescriptor for the event
     * @param timelinePane The Pane used for the timeline
     * @param player This is the controller for playing event. It is used for
     * timeline, to inform the controller for changes in the event through the
     * timeline
     */
    public MediaPlayerCreator(final MediaDescriptor descriptor, final Pane timelinePane, final SyncPlayer player) {
        log.info("Creating MediaPlayerCreator");
        try {
            log.info("Creating media instances");
            this.players = new HashMap();
            this.descriptor = descriptor;
            for (Video video : descriptor.getVideos()) {
                players.put(video.getId(), new MediaPlayer(new Media(new File(video.getSrc()).toURI().toURL().toString())));
            }
            for (Audio audio : descriptor.getAudios()) {
                players.put(audio.getId(), new MediaPlayer(new Media(new File(audio.getSrc()).toURI().toURL().toString())));
            }
            master = descriptor.getMaster();
            masterPlayer = new MediaPlayer(new Media(new File(master.getSrc()).toURI().toURL().toString()));
            log.info("Waiting 1 sec to ensure master media metadata is read to know about its total duration");
            Thread.sleep(1000);
            log.info("Creating the timeline");
            timeline = new Timeline(timelinePane, descriptor.getAnnotation(), new Duration(master.getExplicitDur()), player);
            HBox.setHgrow(timelinePane, Priority.ALWAYS);
        } catch (MalformedURLException ex) {
            Logger.getLogger(MediaPlayerCreator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(MediaPlayerCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
        log.info("MediaPlayerCreator created");
    }

    /**
     * Return the MediaPlayer for the media name informed
     *
     * @param name The name of the media, key for map
     * @return the current MediaPlayer or ends up in an Exception if no key is
     * found. Exception not treated
     */
    public MediaPlayer getPlayer(int name) {
        log.info("Getting player for id " + name);
        return players.get(name);
    }

    /**
     * Retrieve the MediaPlayer for the master media
     *
     * @return master's MediaPlayer
     */
    public MediaPlayer getMasterPlayer() {
        log.info("Getting master player");
        return masterPlayer;
    }

    /**
     * Returns the total duration of the event
     *
     * @return The Duration of the event
     */
    public Duration getDuration() {
        log.info("Getting total duration");
        return masterPlayer.getMedia().getDuration();
    }

    /**
     * Returns the timeline object created for the event
     *
     * @return
     */
    public Timeline getTimeline() {
        log.info("Gettting timeline");
        return timeline;
    }

}
