package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.utils.Time;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

/**
 * SyncPlayer implementation for 2 videos, 1 audio and 1 annotation. This class
 * creates the views for the media, allocating it in the space reserved in the
 * player. It also controls the synchronization of the media.
 *
 * @author Luiz
 */
public class DefaultSyncPlayer extends SyncPlayer {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(DefaultSyncPlayer.class.getName());

    /**
     * ID for video 1
     */
    private int player1ID;
    /**
     * ID for video 2
     */
    private int player2ID;
    /**
     * ID for Audio
     */
    private int playerAudioID;
    /**
     * MediaPlayer that controls video 1 media
     */
    private MediaPlayer player1;
    /**
     * MediaPlayer that controls video 2 media
     */
    private MediaPlayer player2;
    /**
     * MediaPlayer that controls audio media
     */
    private MediaPlayer playerAudio;
    /**
     * MediaPlayer that controls the Master media, an empty audio file with the
     * duration that aggregates all media
     */
    private MediaPlayer masterPlayer;
    /**
     * MediaView to show video 1 media in GUI
     */
    private MediaView view1;
    /**
     * MediaView to show video 2 media in GUI
     */
    private MediaView view2;
    /**
     * Pane to receive the MediaView for video 1
     */
    private Pane video1;
    /**
     * Pane to receive the MediaView for video 2
     */
    private Pane video2;

    /**
     * GUI elements from PlayerController
     */
    private final Slider timeSlider;
    private final Slider volumeSlider;
    private final Label playTime;

    //Bug on resize, variable to adjust the size on the first event of the player
    boolean f = false;

    /**
     * Default constructor. It creates the view for the videos and players for
     * all media. Also the volume and time sliders are prepared with listeners.
     *
     * @param descriptor MediaDescriptor with all media information
     * @param timelinePane The Pane in the scene ready to receive the timeline
     * view
     * @param playerView The Pane in the scene ready to receive the all media
     * views
     * @param timeSlider The slider that represents the ongoing of all media
     * @param volumeSlider The slider that controls the volume of the audio
     * @param playerTime The Label that shows the current time of the event
     * being played in the format mm:ss
     * @param play_pause Button to control the play and pause of the player, its
     * instance is needed so the right action is registered
     */
    public DefaultSyncPlayer(MediaDescriptor descriptor, Pane timelinePane, HBox playerView, final Slider timeSlider, final Slider volumeSlider, Label playerTime, Button play_pause) {
        super(descriptor, timelinePane, playerView, timeSlider, volumeSlider, playerTime, play_pause);
        log.info("Starting DefaultPlayer");
        descriptor.prepareFirstProfile();
        this.timeSlider = timeSlider;
        this.volumeSlider = volumeSlider;
        this.playTime = playerTime;
        log.info("Checking media for 2 videos, 1 audio and 1 annotation");
        if (descriptor.getVideos().size() < 2 || descriptor.getAnnotation().getAnnotations() == null || descriptor.getAudios().size() < 1) {
            log.log(Level.SEVERE, "There's not enough media for this player mode");
        } else {
            log.info("Starting MediaPlayer and MediaView for video 1, 2 and audio");
            video1 = new Pane();
            video2 = new Pane();
            video1.setPrefHeight(200);
            video1.setPrefWidth(200);
            video2.setPrefHeight(200);
            video2.setPrefWidth(200);
            HBox.setHgrow(video1, Priority.ALWAYS);
            HBox.setHgrow(video2, Priority.ALWAYS);
            playerView.getChildren().add(video1);
            playerView.getChildren().add(video2);
            player1ID = descriptor.getVideos().get(0).getId();
            player2ID = descriptor.getVideos().get(1).getId();
            playerAudioID = descriptor.getAudios().get(0).getId();
            player1 = getPlayerContainer().getPlayer(player1ID);
            player2 = getPlayerContainer().getPlayer(player2ID);
            masterPlayer = getPlayerContainer().getMasterPlayer();
            playerAudio = getPlayerContainer().getPlayer(playerAudioID);
            view1 = new MediaView(player1);
            view2 = new MediaView(player2);
            video1.getChildren().add(view1);
            video2.getChildren().add(view2);

            //Listeners to resize media view with window's resize
            video1.widthProperty().addListener(getVideoWidthListener(view1, video1));
            video1.heightProperty().addListener(getVideoHeightListener(view1, video1));
            video2.widthProperty().addListener(getVideoWidthListener(view2, video2));
            video2.heightProperty().addListener(getVideoHeightListener(view2, video2));

            //Listener to reposition the video when the user interacts with the slider
            timeSlider.valueProperty().addListener(new InvalidationListener() {
                public void invalidated(Observable ov) {
                    if (timeSlider.isValueChanging()) {
                        // multiply duration by percentage calculated by slider position
                        if (getPlayerContainer().getDuration() != null) {
                            seek(getPlayerContainer().getDuration().multiply(timeSlider.getValue() / 100.0));
                            getPlayerContainer().getTimeline().animateTo((int) (timeSlider.getValue() / 1000.0));
                        }
                        updateValues();
                    }
                }
            });

            volumeSlider.valueProperty().addListener(new InvalidationListener() {

                @Override
                public void invalidated(Observable o) {
                    if (volumeSlider.isValueChanging()) {
                        playerAudio.setVolume(volumeSlider.getValue() / 100);
                    }
                }
            });
            log.info("DefaultPlayer started");
        }
    }

    /**
     * Video flow control. This method is called whenever an event occurs for
     * the main video.
     */
    @Override
    public void updateValues() {
        if (!f) {
            log.info("First GUI update");
            f = true;
            view1.setFitWidth(video1.getWidth());
            view1.setTranslateX((video1.getWidth() - view1.prefWidth(-1)) / 2);
            view1.setTranslateY((video1.getHeight() - view1.prefHeight(-1)) / 2);
            view1.setFitHeight(video1.getHeight());
            view2.setFitWidth(video2.getWidth());
            view2.setTranslateX((video2.getWidth() - view2.prefWidth(-1)) / 2);
            view2.setTranslateY((video2.getHeight() - view2.prefHeight(-1)) / 2);
            view2.setFitHeight(video2.getHeight());
            view1.setTranslateX((video1.getWidth() - view1.prefWidth(-1)) / 2);
            view1.setTranslateY((video1.getHeight() - view1.prefHeight(-1)) / 2);
            view2.setTranslateX((video2.getWidth() - view2.prefWidth(-1)) / 2);
            view2.setTranslateY((video2.getHeight() - view2.prefHeight(-1)) / 2);
        }

        log.info("Updating GUI elements");
        if (playTime != null && timeSlider != null && volumeSlider != null && getPlayerContainer().getTimeline() != null) {
            Platform.runLater(new Runnable() {
                public void run() {
                    Duration currentTime = masterPlayer.getCurrentTime();
                    playTime.setText(Time.formatTime(currentTime, masterPlayer.getMedia().getDuration()));
                    timeSlider.setDisable(masterPlayer.getMedia().getDuration().isUnknown());
                    if (!timeSlider.isDisabled() && masterPlayer.getMedia().getDuration().greaterThan(Duration.ZERO) && !timeSlider.isValueChanging()) {
                        timeSlider.setValue(currentTime.divide(masterPlayer.getMedia().getDuration()).toMillis() * 100.0);
                        getPlayerContainer().getTimeline().animateTo((int) Math.floor(currentTime.toMillis()));
                    }
                    if (!volumeSlider.isValueChanging()) {
                        volumeSlider.setValue((int) Math.round(playerAudio.getVolume() * 100));
                    }
                }
            });
        }
        log.info("GUI update done");
    }

    /**
     * Play all media
     */
    @Override
    public void play() {
        log.info("Starting the player");
        masterPlayer.play();
        syncTimeChanged();
        log.info("player started");
    }

    /**
     * Pause all media
     */
    @Override
    public void pause() {
        log.info("Pausing the player");
        masterPlayer.pause();
        player1.pause();
        player2.pause();
        playerAudio.pause();
        log.info("Player paused");
    }

    /**
     * Seek in all media
     *
     * @param duration time to be seek
     */
    @Override
    public void seek(Duration duration) {
        log.info("Seeking player");
        masterPlayer.seek(duration);
        syncSeek(duration);
        getPlayerContainer().getTimeline().animateTo((int) duration.toMillis());
        syncTimeChanged();
        log.info("Seek complete");
    }

    /**
     * Update call to decide whether a media needs to start to respect the
     * synchronization
     */
    @Override
    public void syncTimeChanged() {
        log.info("Analyzing media player sync");
        Duration d = masterPlayer.getCurrentTime();
        if (player1.getStatus() != MediaPlayer.Status.PLAYING) {
            Duration test = new Duration(getMediaDescriptor().getMaster().getAreaStartTime(player1ID));
            if (d.greaterThanOrEqualTo(test) && masterPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                log.info("Starting video 1");
                player1.play();
            }
        } else {
            Duration test = new Duration(getMediaDescriptor().getMaster().getAreaEndTime(player1ID));
            if (d.greaterThanOrEqualTo(test) && masterPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                log.info("Pausing video 1");
                player1.pause();
                player1.seek(Duration.ZERO);
            }
        }
        if (player2.getStatus() != MediaPlayer.Status.PLAYING) {
            Duration test = new Duration(getMediaDescriptor().getMaster().getAreaStartTime(player2ID));
            if (d.greaterThanOrEqualTo(test) && masterPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                log.info("Starting video 2");
                player2.play();
            }
        } else {
            Duration test = new Duration(getMediaDescriptor().getMaster().getAreaEndTime(player2ID));
            if (d.greaterThanOrEqualTo(test) && masterPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                log.info("Pausing video 2");
                player2.pause();
                player2.seek(Duration.ZERO);
            }
        }
        if (playerAudio.getStatus() != MediaPlayer.Status.PLAYING) {
            Duration test = new Duration(getMediaDescriptor().getMaster().getAreaStartTime(playerAudioID));
            if (d.greaterThanOrEqualTo(test) && masterPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                log.info("Playing audio");
                playerAudio.play();
            }
        } else {
            log.info("Analyzing audio");
            Duration test = new Duration(getMediaDescriptor().getMaster().getAreaEndTime(playerAudioID));
            log.log(Level.INFO, "hey : {0}", getMediaDescriptor().getMaster().getAreaEndTime(playerAudioID));
            log.log(Level.INFO, "id : {0}", playerAudioID);
            if (d.greaterThanOrEqualTo(test) && masterPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                playerAudio.pause();
                playerAudio.seek(Duration.ZERO);
            }
        }
    }

    /**
     * Adjust the seek for all media, respecting the synchronization. If the
     * media is playing, then the seek goes to duration, if not, the media is
     * paused and the seek goes to the beginning
     *
     * @param duration current time used to determine whether the media on which
     * should be used the seek method
     */
    @Override
    public void syncSeek(Duration duration) {
        log.info("Synchronizing all media");
        Duration d = new Duration(getMediaDescriptor().getMaster().getAreaStartTime(player1ID));
        if (duration.greaterThanOrEqualTo(d)) {
            log.info("Seeking video 1");
            d = new Duration(duration.toMillis() - getMediaDescriptor().getMaster().getAreaStartTime(player1ID));
            player1.seek(d);
        } else {
            log.info("Pausing video 1");
            player1.pause();
            player1.seek(Duration.ZERO);
        }
        d = new Duration(getMediaDescriptor().getMaster().getAreaStartTime(player2ID));
        if (duration.greaterThanOrEqualTo(d)) {
            log.info("Seeking video 2");
            d = new Duration(duration.toMillis() - getMediaDescriptor().getMaster().getAreaStartTime(player2ID));
            player2.seek(d);
        } else {
            log.info("Pausing video 2");
            player2.pause();
            player2.seek(Duration.ZERO);
        }
        d = new Duration(getMediaDescriptor().getMaster().getAreaStartTime(playerAudioID));
        if (duration.greaterThanOrEqualTo(d)) {
            log.info("Seeking audio");
            d = new Duration(duration.toMillis() - getMediaDescriptor().getMaster().getAreaStartTime(playerAudioID));
            playerAudio.seek(d);
        } else {
            log.info("Pausing audio");
            playerAudio.pause();
            playerAudio.seek(Duration.ZERO);
        }
        log.info("Synchronization complete");
    }

    /**
     * Callback from the Javascript in the Timeline. The Javascript doesn't
     * recognize the Java Duration object, sending only a double value.
     *
     * @param millis the time in milliseconds
     */
    @Override
    public void seekToMillis(double millis) {
        log.info("Synchronizing from Javascript");
        seek(new Duration(millis));
    }
}
