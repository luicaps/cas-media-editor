package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.view.playerView.CASMediaEditor;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

/**
 * Abstract class to represent the event player controller. It has to be
 * implemented for new types of player, using different set of media
 *
 * @author Luiz
 */
public abstract class SyncPlayer {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(SyncPlayer.class.getName());

    /**
     * MediaDescriptor is the class with the metadada of all media
     */
    private final MediaDescriptor descriptor;
    /**
     * MediaPlayerCreator instance for the current event, holding all media
     * player and the timeline
     */
    private final MediaPlayerCreator playerContainer;
    /**
     * stop request player control flag
     */
    private boolean stopRequested;
    /**
     * end request player control flag
     */
    private boolean atEndOfMedia;
    /**
     * Button to control play and pause of the event
     */
    private final Button play_pause;

    /**
     * Play GUI icon
     */
    private final Image PlayButtonImage = new Image(CASMediaEditor.class.getResourceAsStream("playbutton.png"));
    /**
     * Pause GUI icon
     */
    private final Image PauseButtonImage = new Image(CASMediaEditor.class.getResourceAsStream("pausebutton.png"));
    /**
     * ImageView for {@link SyncPlayer#PlayButtonImage
     */
    private final ImageView imageViewPlay = new ImageView(PlayButtonImage);
    /**
     * ImageView for {@link SyncPlayer#PauseButtonImage
     */
    private final ImageView imageViewPause = new ImageView(PauseButtonImage);

    /**
     * Default constructor, receives all the information needed, starts the
     * MediaPlayerCreator and prepare the listeners for the master media
     *
     * @param descriptor MediaDescriptor with all media information
     * @param timelinePane The Pane in the scene ready to receive the timeline
     * view
     * @param playerView The Pane in the scene ready to receive the all media
     * views
     * @param timeSlider The slider that represents the ongoing of all media
     * @param volumeSlider The slider that controls the volume of the audio
     * @param playerTime The Label that shows the current time of the event
     * being played in the format mm:ss
     * @param play_pause Button to control the play and pause of the player, its
     * instance is needed so the right action is registered
     */
    public SyncPlayer(MediaDescriptor descriptor, Pane timelinePane, HBox playerView, Slider timeSlider, Slider volumeSlider, Label playerTime, Button play_pause) {
        log.info("Creating SyncPlayer instance");
        this.descriptor = descriptor;
        this.play_pause = play_pause;
        playerContainer = new MediaPlayerCreator(descriptor, timelinePane, this);
        prepareMasterPlayer();
        log.info("SyncPlayer instance created");
    }

    /**
     * Returns the MediaDescriptor for the current event
     *
     * @return MediaDescriptor of the event
     */
    public MediaDescriptor getMediaDescriptor() {
        log.info("Getting media descriptor");
        return descriptor;
    }

    /**
     * Returns the MediaPlayerCreator of the current event
     *
     * @return MediaPlayerCreator of the event
     */
    public MediaPlayerCreator getPlayerContainer() {
        log.info("Getting player container");
        return playerContainer;
    }

    /**
     * Abstract method to implement the play logic
     */
    public abstract void play();

    /**
     * Abstract method to implement the pause logic
     */
    public abstract void pause();

    /**
     * Abstract method to implement the seek logic
     *
     * @param duration time to be used on seek call
     */
    public abstract void seek(Duration duration);

    /**
     * Abstract method to implement the seek logic using a double value
     *
     * @param millis time to be used on seek call in milliseconds
     */
    public abstract void seekToMillis(double millis);

    /**
     * Abstract methoc to implement the synchronization logic during the
     * execution of the event. It is intended to analyze and start all media in
     * it's correct time
     */
    public abstract void syncTimeChanged();

    /**
     * Abstract method to implement the synchronization after the seek call. IT
     * NEEDS TO BE CALLED ON seek(); after the seek logic.
     *
     * @param duration time to used on seek
     */
    public abstract void syncSeek(Duration duration);

    /**
     * Updates all interface elements with information of the event
     */
    public abstract void updateValues();

    /**
     * Action logic for the play button
     */
    public void playPause() {
        log.info("Pause player call");
        updateValues();
        MediaPlayer masterPlayer = playerContainer.getMasterPlayer();
        MediaPlayer.Status status = masterPlayer.getStatus();
        if (status == MediaPlayer.Status.UNKNOWN
                || status == MediaPlayer.Status.HALTED) {
            // don't do anything in these states
            return;
        }

        if (status == MediaPlayer.Status.PAUSED
                || status == MediaPlayer.Status.READY
                || status == MediaPlayer.Status.STOPPED) {
            // rewind the movie if we're sitting at the end
            if (atEndOfMedia) {
                seek(masterPlayer.getStartTime());
                atEndOfMedia = false;
                play_pause.setGraphic(imageViewPlay);
                updateValues();
            }
            play();
            play_pause.setGraphic(imageViewPause);
        } else {
            pause();
        }
        log.info("Pause call complete");
    }

    /**
     * return the listener for width change, to resize the media view
     *
     * @param media media to be resized
     * @param parent parent Pane to retrieve width information
     * @return the change listener
     */
    public ChangeListener<Number> getVideoWidthListener(final MediaView media, final Pane parent) {
        return new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                double width = t1.doubleValue();
                media.setFitWidth(width);
                media.setTranslateX((width - media.prefWidth(-1)) / 2);
                media.setTranslateY((parent.getHeight() - media.prefHeight(-1)) / 2);
            }
        };
    }

    /**
     * return the listener for height change, to resize the media view
     *
     * @param media media to be resized
     * @param parent parent Pane to retrieve height information
     * @return the change listener
     */
    public ChangeListener<Number> getVideoHeightListener(final MediaView media, final Pane parent) {
        return new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                double height = t1.doubleValue();
                media.setFitHeight(height);
                media.setTranslateY((height - media.prefHeight(-1)) / 2);
                media.setTranslateX((parent.getWidth() - media.prefWidth(-1)) / 2);
            }
        };
    }

    /**
     * Prepare the change listeners for the master player
     */
    public void prepareMasterPlayer() {
        log.info("Preparing Master Player");
        playerContainer.getMasterPlayer().currentTimeProperty().addListener(new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> ov, Duration t, Duration t1) {
                syncTimeChanged();
                updateValues();
            }
        });
        playerContainer.getMasterPlayer().setOnPlaying(new Runnable() {
            public void run() {
                if (stopRequested) {
                    pause();
                    stopRequested = false;
                } else {
                    play_pause.setGraphic(imageViewPause);
                }
            }
        });
        playerContainer.getMasterPlayer().setOnPaused(new Runnable() {
            public void run() {
                play_pause.setGraphic(imageViewPlay);
            }
        });
        playerContainer.getMasterPlayer().setOnReady(new Runnable() {
            public void run() {
                updateValues();
            }
        });

        playerContainer.getMasterPlayer().setOnEndOfMedia(new Runnable() {
            public void run() {
                play_pause.setGraphic(imageViewPlay);
                stopRequested = true;
                atEndOfMedia = true;
            }
        });
        log.info("Master Player prepared");
    }

}
