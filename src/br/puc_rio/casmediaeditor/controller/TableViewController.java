package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.model.AnnotationItem;
import br.puc_rio.casmediaeditor.utils.Time;
import br.puc_rio.casmediaeditor.utils.TimeTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javafx.util.Duration;

/**
 * FXML Controller class to show the table of annotations and the control to add
 * new ones.
 *
 * @author Luiz
 */
public class TableViewController implements Initializable {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(TableViewController.class.getName());

    /**
     * String representation of regex to verify if it's hour in "HH:mm:ss.SSS"
     * format
     */
    private final StringProperty hour = new SimpleStringProperty("[0-9]||"
            + "[0-9]{2}||"
            + "[0-9]{2}:||"
            + "[0-9]{2}:[0-9]||"
            + "[0-9]{2}:[0-9]{2}||"
            + "[0-9]{2}:[0-9]{2}:||"
            + "[0-9]{2}:[0-9]{2}:[0-9]||"
            + "[0-9]{2}:[0-9]{2}:[0-9]{2}||"
            + "[0-9]{2}:[0-9]{2}:[0-9]{2}.||"
            + "[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]||"
            + "[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{2}||"
            + "[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}");
    /**
     * String representation of regex to verify if it's minute in "mm:ss.SSS"
     * format
     */
    private final StringProperty minute = new SimpleStringProperty("[0-9]||"
            + "[0-9]{2}||"
            + "[0-9]{2}:||"
            + "[0-9]{2}:[0-9]||"
            + "[0-9]{2}:[0-9]{2}||"
            + "[0-9]{2}:[0-9]{2}.||"
            + "[0-9]{2}:[0-9]{2}.[0-9]||"
            + "[0-9]{2}:[0-9]{2}.[0-9]{2}||"
            + "[0-9]{2}:[0-9]{2}.[0-9]{3}");

    /**
     * Variables linked to the GUI elements from FXML.
     */
    @FXML
    TableView table;
    @FXML
    TableColumn start;
    @FXML
    TableColumn end;
    @FXML
    TableColumn content;
    @FXML
    TableColumn tableSync;
    @FXML
    TextField nContent;
    @FXML
    TextField nEnd;
    @FXML
    TextField nStart;
    @FXML
    Button nAdd;

    /**
     * Icon for the sync button in the table presented for each annotation
     */
    private static final Image syncIcon = new Image(TableViewController.class.getResourceAsStream("/br/puc_rio/casmediaeditor/view/icon/live_sync.png"));
    /**
     * SyncPlayer instance to receive the change calls
     */
    SyncPlayer player;
    /**
     * MediaDescriptor to receive the change calls so that the data is changed
     * for saving
     */
    MediaDescriptor desc;
    /**
     * Annotations set converted to be presented in the interface. This is a
     * minor problem where the annotations are replicated throughout the
     * interface, which requires a lot of calls to different places to indicate
     * change
     */
    ObservableList<AnnotationItem> annotations;

    /**
     * Receive the initial data to fetch the table
     *
     * @param desc
     */
    public void putData(MediaDescriptor desc) {
        log.info("Receiving annotation data");
        this.desc = desc;
        ArrayList<AnnotationItem> list = desc.getAnnotation().getAnnotations();
        annotations = FXCollections.observableArrayList(list);
        table.setItems(annotations);
        log.info("Table filled");
    }

    public void setPlayer(final SyncPlayer player) {
        log.info("Setting SynPlayer to watch for time change");
        this.player = player;
        player.getPlayerContainer().getMasterPlayer().currentTimeProperty().addListener(new ChangeListener<Duration>() {

            @Override
            public void changed(ObservableValue<? extends Duration> ov, Duration t, Duration t1) {
                nEnd.setText(Time.formatTimeMinMs(player.getPlayerContainer().getMasterPlayer().getCurrentTime()));
            }
        });
        log.info("Listener registered into master player");
    }

    /**
     * Call from action button in the GUI to add new annotation
     *
     * @param event
     */
    @FXML
    protected void handleNAddButtonAction(ActionEvent event) {
        log.log(Level.INFO, "New item for: {0}, {1}, {2}", new Object[]{nStart.getText(), nEnd.getText(), nContent.getText()});
        AnnotationItem item = new AnnotationItem();
        item.setContent(nContent.getText());
        item.setStart(nStart.getText());
        item.setEnd(nEnd.getText());
        desc.setNextIdFor(item);
        desc.getAnnotation().getAnnotations().add(item);
        desc.sortAnnotation();
        annotations = FXCollections.observableArrayList(desc.getAnnotation().getAnnotations());
        table.setItems(annotations);
        player.getPlayerContainer().getTimeline().addData(item);
        nContent.setText("");
    }

    /**
     * Initializes the controller after JavaFX created everything necessary
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        log.info("Initializing");
        start.sortableProperty().set(false);
        end.sortableProperty().set(false);
        content.sortableProperty().set(false);
        tableSync.sortableProperty().set(false);
        nStart.setText("00:00.000");

        log.info("Generating table instance");
        table.setEditable(true);
        Callback<TableColumn, TableCell> timeCellFactory = new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn p) {
                return new TextEditingCell();
            }
        };

        Callback<TableColumn, TableCell> textCellFactory = new Callback<TableColumn, TableCell>() {

            @Override
            public TableCell call(TableColumn p) {
                return new TextEditingCell();
            }
        };

        start.setCellFactory(timeCellFactory);
        start.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<AnnotationItem, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<AnnotationItem, String> t) {
                AnnotationItem item = ((AnnotationItem) t.getTableView().getItems().get(t.getTablePosition().getRow()));
                item.setStart(t.getNewValue());
                player.getPlayerContainer().getTimeline().changeItemStart(item.getId(), item.getStartMillis());
                desc.getMaster().insertArea(item.getId(), item.getContent(), item.getStartMillis(), item.getEndMillis());
                player.syncTimeChanged();
            }
        });

        end.setCellFactory(timeCellFactory);
        end.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<AnnotationItem, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<AnnotationItem, String> t) {
                if (t.getNewValue().matches(hour.get()) || t.getNewValue().matches(minute.get())) {
                    AnnotationItem item = ((AnnotationItem) t.getTableView().getItems().get(t.getTablePosition().getRow()));
                    item.setEnd(t.getNewValue());
                    player.getPlayerContainer().getTimeline().changeItemEnd(item.getId(), item.getEndMillis());
                    desc.getMaster().insertArea(item.getId(), item.getContent(), item.getStartMillis(), item.getEndMillis());
                    player.syncTimeChanged();
                }
            }
        });

        content.setCellFactory(textCellFactory);
        content.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<AnnotationItem, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<AnnotationItem, String> t) {
                AnnotationItem item = ((AnnotationItem) t.getTableView().getItems().get(t.getTablePosition().getRow()));
                item.setContent(t.getNewValue());
                player.getPlayerContainer().getTimeline().changeItemContent(item.getId(), item.getContent());
                desc.getMaster().insertArea(item.getId(), item.getContent(), item.getStartMillis(), item.getEndMillis());
                desc.changeIdName(t.getOldValue(), t.getNewValue());
            }
        });

        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                TablePosition focusedCell = table.getFocusModel().getFocusedCell();
                if (focusedCell.getRow() == -1 || focusedCell.getColumn() == -1) {
                    return;
                }
                int col = focusedCell.getColumn();
                AnnotationItem item = (AnnotationItem) annotations.get(focusedCell.getRow());
                player.getPlayerContainer().getTimeline().changeSelection(item.getId());
                switch (col) {
                    case 0:
                    case 1:
                        table.edit(focusedCell.getRow(), focusedCell.getTableColumn());
                        break;
                    case 2:
                        table.edit(focusedCell.getRow(), focusedCell.getTableColumn());
                        break;
                    case 3:
                        table.edit(focusedCell.getRow(), focusedCell.getTableColumn());
                        break;
                }

            }
        });

        tableSync.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AnnotationItem, Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<AnnotationItem, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        tableSync.setCellFactory(new Callback<TableColumn<AnnotationItem, Boolean>, TableCell<AnnotationItem, Boolean>>() {

            @Override
            public TableCell<AnnotationItem, Boolean> call(TableColumn<AnnotationItem, Boolean> p) {
                return new ButtonCell(table);
            }

        });
        
        log.info("Table GUI initialization complete");

    }

    /**
     * Class that defines the editing cell for {@link TimeEditingCell} in the table
     */
    private class TimeEditingCell extends TableCell<AnnotationItem, String> {
        //TODO Use this TextField
        private TimeTextField textField;

        @Override
        public void startEdit() {
            super.startEdit();
            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.requestFocus();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            textField = null;
            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    textField.requestFocus();
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
//            textField = new TextField(getString()) {
//                @Override
//                public void replaceText(int start, int end, String text) {
//                    if (text.matches("[0-9]|.")) {
//                        super.replaceText(start, end, text);
//                    }
//                }
//
//                @Override
//                public void replaceSelection(String text) {
//                    if (text.matches("[0-9]|.")) {
//                        super.replaceSelection(text);
//                    }
//                }
//            };
            textField = new TimeTextField();
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        try {
                            commitEdit(textField.getText());
                        } catch (NumberFormatException e) {
                            cancelEdit();
                        }
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    /**
     * Class that define a general TextField editing cell in the table
     */
    private class TextEditingCell extends TableCell<AnnotationItem, String> {

        private TextField textField;

        @Override
        public void startEdit() {
            super.startEdit();
            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.requestFocus();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            textField = null;
            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    textField.requestFocus();
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    /**
     * Class that defines a button cell in the table
     */
    private class ButtonCell extends TableCell<AnnotationItem, Boolean> {

        private final Button cellButton = new Button();

        ButtonCell(final TableView table) {
            cellButton.setGraphic(new ImageView(syncIcon));
            this.setAlignment(Pos.CENTER);
            this.widthProperty().addListener(new ChangeListener<Number>() {

                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                    cellButton.setMinWidth(t1.doubleValue());
                }
            });
            cellButton.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent t) {
                    AnnotationItem ob = (AnnotationItem) table.getItems().get(getTableRow().getIndex());
                    player.seek(new Duration(ob.getStartMillis()));
                }
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    public TableView getTable() {
        return table;
    }

    public TableColumn getStart() {
        return start;
    }

    public TableColumn getEnd() {
        return end;
    }

    public TableColumn getContent() {
        return content;
    }

    public TableColumn getTableSync() {
        return tableSync;
    }

    public TextField getnContent() {
        return nContent;
    }

    public TextField getnEnd() {
        return nEnd;
    }

    public TextField getnStart() {
        return nStart;
    }

    public Button getnAdd() {
        return nAdd;
    }

    public ObservableList<AnnotationItem> getAnnotations() {
        return annotations;
    }

}
