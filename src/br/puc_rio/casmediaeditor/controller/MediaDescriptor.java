package br.puc_rio.casmediaeditor.controller;

import br.puc_rio.casmediaeditor.model.Annotation;
import br.puc_rio.casmediaeditor.model.AnnotationItem;
import br.puc_rio.casmediaeditor.model.Audio;
import br.puc_rio.casmediaeditor.model.Master;
import br.puc_rio.casmediaeditor.model.Video;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Controller class that read all the NCL eventDescription and creates all
 * necessary model objects
 *
 * @author Luiz
 */
public class MediaDescriptor {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(MediaDescriptor.class.getName());
    
    /**
     * Set of video described in evenDescription
     */
    ArrayList<Video> videos = new ArrayList();
    /**
     * Set of audio described in eventDescription
     */
    ArrayList<Audio> audios = new ArrayList();
    /**
     * Master media described in eventDescription
     */
    Master master;
    /**
     * Annotation set described in eventDescription
     */
    Annotation annotation = new Annotation();
    /**
     * Root path for the event in the file system
     */
    String root;
    /**
     * ID of the event described in the evenDescription
     */
    String eventId;
    /**
     * Counter auxiliary variable to define an unique id for all media to be
     * described
     */
    private int idCount = 0;

    /**
     * Getter for {@link MediaDescriptor#videos}
     * @return the {@link MediaDescriptor#videos} set
     */
    public ArrayList<Video> getVideos() {
        return videos;
    }

    /**
     * Getter for {@link MediaDescriptor#audios}
     * @return the {@link MediaDescriptor#audios} set
     */
    public ArrayList<Audio> getAudios() {
        return audios;
    }

    /**
     * Getter for {@link MediaDescriptor#annotation}
     * @return the {@link MediaDescriptor#annotation} set
     */
    public Annotation getAnnotation() {
        return annotation;
    }

    /**
     * Getter for {@link MediaDescriptor#master}
     * @return the Master model object instance
     */
    public Master getMaster() {
        return master;
    }

    /**
     * Default constructor that do all the job of reading the description files
     * @param rootPath the root path of the event in the file system
     */
    public MediaDescriptor(String rootPath, PlayerController player) {
        log.info("Starting MediaDescriptor for path " + rootPath);
        try {
            root = rootPath;
            HashMap<String, Integer> firstIds = new HashMap();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(rootPath + File.separator + "eventDescription.ncl");
            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();
            eventId = root.getAttribute("id");
            NodeList tags = root.getElementsByTagName("media");
            log.info("Preparation complete, ready to iterate the media descriptions");
            for (int temp = 0; temp < tags.getLength(); temp++) {
                Node nNode = tags.item(temp);
                if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String type = eElement.getAttribute("type");
                    String id = eElement.getAttribute("id");
                    if (defineType(type, typeVideo)) {
                        log.log(Level.INFO, "Found a video media with id {0}", id);
                        String videoSrc = rootPath + File.separator + eElement.getAttribute("src");
                        NodeList properties = eElement.getElementsByTagName("property");
                        String startDate = "";
                        String finishDate = "";
                        for (int i = 0; i < properties.getLength(); i++) {
                            Element prop = (Element) properties.item(i);
                            if (prop.getAttribute("name").equals("startDate")) {
                                startDate = prop.getAttribute("value");
                            } else if (prop.getAttribute("name").equals("finishDate")) {
                                finishDate = prop.getAttribute("value");
                            }
                        }
                        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
                        Date s = format.parse(startDate);
                        Date f = format.parse(finishDate);
                        log.log(Level.INFO, "Video media of id {0} reading complete. Creating Video model instance", id);
                        videos.add(new Video(id, videoSrc, s, f));
                    } else if (defineType(type, typeAudio)) {
                        log.log(Level.INFO, "Found an audio media with id {0}", id);
                        audios = new ArrayList();
                        String audioSrc = rootPath + File.separator + eElement.getAttribute("src");
                        NodeList properties = eElement.getElementsByTagName("property");
                        String startDate = "";
                        String finishDate = "";
                        for (int i = 0; i < properties.getLength(); i++) {
                            Element prop = (Element) properties.item(i);
                            if (prop.getAttribute("name").equals("startDate")) {
                                startDate = prop.getAttribute("value");
                            } else if (prop.getAttribute("name").equals("finishDate")) {
                                finishDate = prop.getAttribute("value");
                            }
                        }
                        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
                        Date s = format.parse(startDate);
                        Date f = format.parse(finishDate);
                        log.log(Level.INFO, "Audio media of id {0} reading complete. Creating Audio model instance", id);
                        audios.add(new Audio(id, audioSrc, s, f));
                    } else if (defineType(type, typeAnnotation)) {
                        log.log(Level.INFO, "Found an Annotation media with id {0}", id);
                        try {
                            String src = eElement.getAttribute("src");
                            File srt = new File(rootPath + File.separator + src);
                            BufferedReader reader = new BufferedReader(new FileReader(srt));
                            ArrayList<AnnotationItem> list = new ArrayList();
                            log.info("Reading srt file");
                            while (true) {
                                AnnotationItem annotation = new AnnotationItem();
                                reader.readLine();
                                String[] time = reader.readLine().trim().split("-->");
                                String begin = time[0].trim();
                                String end = time[1].trim();
                                begin = begin.replace(',', '.');
                                time = begin.split(":");
                                double beginSec = ((((Integer.parseInt(time[0]) * 60) + (Integer.parseInt(time[1]))) * 60) + Double.parseDouble(time[2])) * 1000;
                                annotation.setStartMillis(beginSec);
                                end = end.replace(',', '.');
                                time = end.split(":");
                                double endSec = ((((Integer.parseInt(time[0]) * 60) + (Integer.parseInt(time[1]))) * 60) + Double.parseDouble(time[2])) * 1000;
                                annotation.setEndMillis(endSec);
                                String line = reader.readLine();
                                String info = "";
                                while (line != null && !line.equals("")) {
                                    info += line;
                                    line = reader.readLine();
                                }
                                annotation.setContent(info);
                                list.add(annotation);
                                if (line == null) {
                                    break;
                                }
                            }

                            NodeList properties = eElement.getElementsByTagName("property");
                            String startDate = "";
                            String finishDate = "";
                            for (int i = 0; i < properties.getLength(); i++) {
                                Element prop = (Element) properties.item(i);
                                if (prop.getAttribute("name").equals("startDate")) {
                                    startDate = prop.getAttribute("value");
                                } else if (prop.getAttribute("name").equals("finishDate")) {
                                    finishDate = prop.getAttribute("value");
                                }
                            }
                            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
                            Date s = format.parse(startDate);
                            Date f = format.parse(finishDate);
                            annotation.setAnnotations(list);
                            annotation.setStartDate(s);
                            annotation.setFinishDate(f);
                            annotation.setIdName(id);
                            annotation.setOriginalId(id);
                            log.log(Level.INFO, "Annotation media of id {0} reading complete", id);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else if (type.equals("")) {
                        log.log(Level.INFO, "Found the master media with id {0}", id);
                        double ex = 0;
                        String startDate = "";
                        String finishDate = "";
                        String masterSrc = rootPath + File.separator + eElement.getAttribute("src");
                        NodeList properties = eElement.getElementsByTagName("property");
                        for (int i = 0; i < properties.getLength(); i++) {
                            Element prop = (Element) properties.item(i);
                            if (prop.getAttribute("name").equals("eventId")) {
                                id = prop.getAttribute("value");
                            } else if (prop.getAttribute("name").equals("explicitDur")) {
                                String exp = prop.getAttribute("value");
                                exp = exp.substring(0, exp.length() - 1);
                                ex = Double.parseDouble(exp);
                            } else if (prop.getAttribute("name").equals("startDate")) {
                                startDate = prop.getAttribute("value");
                            } else if (prop.getAttribute("name").equals("finishDate")) {
                                finishDate = prop.getAttribute("value");
                            }
                        }
                        if ((ex - ((int) ex)) > 0) {
                            ex = ((int) ex) + 1;
                        }
                        ex *= 1000;
                        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
                        Date s = format.parse(startDate);
                        Date f = format.parse(finishDate);
                        log.log(Level.INFO, "Master media of id {0} preparation complete. Creating new Master instance", id);
                        master = new Master(id, ex, s, f, masterSrc);

                        log.info("Reading master media areas");
                        NodeList area = eElement.getElementsByTagName("area");
                        for (int i = 0; i < area.getLength(); i++) {
                            Element ar = (Element) area.item(i);
                            String exp = ar.getAttribute("begin");
                            exp = exp.substring(0, exp.length() - 1);
                            double begin = Double.parseDouble(exp);
                            exp = ar.getAttribute("end");
                            exp = exp.substring(0, exp.length() - 1);
                            double end = Double.parseDouble(exp);
                            String iD = ar.getAttribute("id");
                            firstIds.put(iD, idCount++);
                            master.insertArea(firstIds.get(iD), iD, (begin * 1000), (end * 1000));
                        }
                        log.info("Master media Ready");
                    }
                }
            }
            log.info("Adjusting annotations");
            adjustAnnotationByMaster(annotation, master);
            
            log.info("Changing media id according to the current area id in the master media");
            for (Video video : videos) {
                if (firstIds.containsKey(("a" + video.getIdName()))) {
                    video.setId(firstIds.get(("a" + video.getIdName())));
                }
            }
            for (Audio audio : audios) {
                if (firstIds.containsKey(("a" + audio.getIdName()))) {
                    audio.setId(firstIds.get(("a" + audio.getIdName())));
                }
            }
            if (firstIds.containsKey(("a" + annotation.getIdName()))) {
                annotation.setId(firstIds.get(("a" + annotation.getIdName())));
            }
            log.info("Constructor completed");
        } catch (Exception ex) {
            Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
            player.fileErrorMessage();
        }
    }

    /**
     * Change the id name for a Video or Audio media
     * @param oldVal old id value
     * @param newVal new id value
     */
    public void changeIdName(String oldVal, String newVal) {
        log.log(Level.INFO, "Changing id name from {0} to {1}", new Object[]{oldVal, newVal});
        for (Video video : videos) {
            if (video.getIdName().equals(oldVal)) {
                log.log(Level.INFO, "Changing for video {0}", video.getId());
                video.setIdName(newVal);
                return;
            }
        }
        for (Audio audio : audios) {
            if (audio.getIdName().equals(oldVal)) {
                log.log(Level.INFO, "Changing for audio {0}", audio.getId());
                audio.setIdName(newVal);
            }
        }
    }

    /**
     * Prepare two videos and one audio media as an annotation to be presented in the timeline. This is the first profile considered  for editing
     */
    public void prepareFirstProfile() {
        log.info("Preparing first profile annotation media tags");
        Video video1 = videos.get(0);
        Video video2 = videos.get(1);
        Audio audio = audios.get(0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(master.getStartDate());
        int masterStart = ((((cal.get(Calendar.MINUTE) * 60) + cal.get(Calendar.SECOND)) * 1000) + cal.get(Calendar.MILLISECOND));
        log.log(Level.INFO, "masterStart: {0}", master.getStartDate());
        log.log(Level.INFO, "CalculateDate: {0}", masterStart);
        log.log(Level.INFO, "explicitDur: {0}", master.getExplicitDur());

        log.info("creating for video 1");

        AnnotationItem an1 = new AnnotationItem();
        an1.setClassName("video");
        an1.setContent(video1.getIdName());
        cal.setTime(video1.getStartDate());
        log.log(Level.INFO, "startDate: {0}", video1.getStartDate());
        int video1Start = ((((cal.get(Calendar.MINUTE) * 60) + cal.get(Calendar.SECOND)) * 1000) + cal.get(Calendar.MILLISECOND));
        log.log(Level.INFO, "CalculateDate: {0}", video1Start);
        cal.setTime(video1.getFinishDate());
        log.log(Level.INFO, "finishDate: {0}", video1.getFinishDate());
        int video1Finish = ((((cal.get(Calendar.MINUTE) * 60) + cal.get(Calendar.SECOND)) * 1000) + cal.get(Calendar.MILLISECOND));
        log.log(Level.INFO, "CalculateDate: {0}", video1Start);
        log.log(Level.INFO, "AnnotationStart: {0}", (video1Start - masterStart));
        log.log(Level.INFO, "AnnotationEnd: {0}", ((video1Finish - video1Start)));
        an1.setStartMillis(video1Start - masterStart);
        an1.setEndMillis((video1Start - masterStart) + (video1Finish - video1Start));

        log.info("creating for Video 2");

        AnnotationItem an2 = new AnnotationItem();
        an2.setClassName("tela");
        an2.setContent(video2.getIdName());
        cal.setTime(video2.getStartDate());
        log.log(Level.INFO, "startDate: {0}", video2.getStartDate());
        int video2Start = ((((cal.get(Calendar.MINUTE) * 60) + cal.get(Calendar.SECOND)) * 1000) + cal.get(Calendar.MILLISECOND));
        log.log(Level.INFO, "CalculateDate: {0}", video2Start);
        cal.setTime(video2.getFinishDate());
        log.log(Level.INFO, "finishDate: {0}", video2.getFinishDate());
        int video2Finish = ((((cal.get(Calendar.MINUTE) * 60) + cal.get(Calendar.SECOND)) * 1000) + cal.get(Calendar.MILLISECOND));
        log.log(Level.INFO, "CalculateDate: {0}", video2Finish);
        log.log(Level.INFO, "AnnotationStart: {0}", (video2Start - masterStart));
        log.log(Level.INFO, "AnnotationEnd: {0}", ((video2Finish - video2Start)));
        an2.setStartMillis(video2Start - masterStart);
        an2.setEndMillis((video2Start - masterStart) + (video2Finish - video2Start));

        log.info("creating Audio");

        AnnotationItem an3 = new AnnotationItem();
        an3.setClassName("audio");
        an3.setContent(audio.getIdName());
        cal.setTime(audio.getStartDate());
        log.log(Level.INFO, "startDate: {0}", audio.getStartDate());
        int audioStart = ((((cal.get(Calendar.MINUTE) * 60) + cal.get(Calendar.SECOND)) * 1000) + cal.get(Calendar.MILLISECOND));
        log.log(Level.INFO, "CalculateDate: {0}", audioStart);
        cal.setTime(audio.getFinishDate());
        log.log(Level.INFO, "finishDate: {0}", audio.getFinishDate());
        int audioFinish = ((((cal.get(Calendar.MINUTE) * 60) + cal.get(Calendar.SECOND)) * 1000) + cal.get(Calendar.MILLISECOND));
        log.log(Level.INFO, "CalculateDate: {0}", audioFinish);
        log.log(Level.INFO, "AnnotationStart: {0}", (audioStart - masterStart));
        log.log(Level.INFO, "AnnotationEnd: {0}", ((audioFinish - audioStart)));
        an3.setStartMillis(audioStart - masterStart);
        an3.setEndMillis((audioStart - masterStart) + (audioFinish - audioStart));
        log.log(Level.INFO, "Audio ID: {0}", audio.getId());

        an1.setId(video1.getId());
        an2.setId(video2.getId());
        an3.setId(audio.getId());
        annotation.getAnnotations().add(an1);
        annotation.getAnnotations().add(an2);
        annotation.getAnnotations().add(an3);
        sortAnnotation();
        log.info("Preparation finished");
    }

    /**
     * Sort all annotations using compareTo method
     */
    public void sortAnnotation() {
        log.info("Sorting annotations");
        Object[] items = annotation.getAnnotations().toArray();
        Arrays.sort(items);
        ArrayList<AnnotationItem> list = new ArrayList();
        for (Object item : items) {
            list.add((AnnotationItem) item);
        }
        annotation.setAnnotations(list);
    }

    /**
     * Set an unique id for an annotation item
     * @param item 
     */
    public void setNextIdFor(AnnotationItem item) {
        log.log(Level.INFO, "Setting next if for content {0}", item.getContent());
        item.setId(idCount++);
    }

    /**
     * adjusts all the annotation starting times to the master media starting time
     * @param annotation the Annotation model instance to be adjusted
     * @param master The master media to use as basis for the adjustment
     */
    public static void adjustAnnotationByMaster(Annotation annotation, Master master) {
        log.log(Level.INFO, "Adjusting annotations from {0} related to master media {1}", new Object[]{annotation.getIdName(), master.getEventId()});
        annotation.adjustAnnotations(master.getAreaStartTime(annotation.getId()));
    }

    /**
     * Method to adjust the audio time related to the given video
     * @deprecated The event is not synchronized using a video as base anymore. Master media is the basis for starting and finishing the event. 
     * {@link MediaDescriptor#prepareFirstProfile()}
     * @param audio
     * @param video 
     */
    public static void adjustAudioByVideo(Audio audio, Video video) {
        log.log(Level.INFO, "Adjusting audio {0} related to video {1}", new Object[]{audio.getIdName(), video.getIdName()});
        Date videoStart = video.getStartDate();
        Date audioStart = audio.getStartDate();
        /*
         Calcutating the difference in the begining of the video compared to annotations, considering that the video always start after annotations
         */
        double d = (audioStart.getTime() - videoStart.getTime());
        audio.setTimeGap(d);
    }

    /**
     * Media type definition related to the type in the NCL eventDescription file
     */
    public static final String typeVideo = "video";
    public static final String typeAudio = "audio";
    public static final String typeSlide = "slide";
    public static final String typeAnnotation = "annotation";

    /**
     * define if some media type is of the expected type
     * @param type type given to be compared
     * @param compare type defined above to use as basis for comparison
     * @return true whether the type given is the type to be compared
     */
    public static boolean defineType(String type, String compare) {
        log.info("Defining type");
        //video types
        if (type.contains("video") && compare.equals(typeVideo)) {
            log.info("Defined as Video");
            return true;
        } //audio types
        else if (type.contains("audio") && compare.equals(typeAudio)) {
            log.info("Defined as Audio");
            return true;
        } //slide types
        else if (type.contains("application") && compare.equals(typeSlide)) {
            log.info("Defined as Application");
            return true;
        } //annotation types
        else if (type.contains("text") && compare.equals(typeAnnotation)) {
            log.info("Defined as Text");
            return true;
        }
        return false;
    }

    /**
     * Save the changes made for the event, getting all annotation information and saving to the master media only. NOT FINISHED YET
     */
    public void saveEvent() {
        log.info("Saving event");
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(root + File.separator + "eventDescription.ncl");
            doc.getDocumentElement().normalize();
            Element rootEl = doc.getDocumentElement();
            NodeList tags = rootEl.getElementsByTagName("media");
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");

            for (int temp = 0; temp < tags.getLength(); temp++) {
                Node nNode = tags.item(temp);
                Element eElement = (Element) nNode;
                String id = eElement.getAttribute("id");
                for (Video video : videos) {
                    if (video.getOriginalId().equals(id)) {
                        AnnotationItem item = annotation.getAnnotationById(video.getId());
                        eElement.setAttribute("id", item.getContent());
                    }
                }
                for (Audio audio : audios) {
                    if (audio.getOriginalId().equals(id)) {
                        AnnotationItem item = annotation.getAnnotationById(audio.getId());
                        eElement.setAttribute("id", item.getContent());
                    }
                }
                if (annotation.getOriginalId().equals(id)) {
                    saveSrt(root);
                }
                if ("casEvent".equals(id)) {
                    NodeList areas = eElement.getElementsByTagName("area");
                    for (int i = 0; i < areas.getLength(); i++) {
                        Element area = (Element) areas.item(i);
                        for (Video video : videos) {
                            if (("a" + video.getOriginalId()).equals(area.getAttribute("id"))) {
                                AnnotationItem item = annotation.getAnnotationById(video.getId());
                                double start = item.getStartMillis();
                                start *= 1000;
                                area.setAttribute("begin", (start + "s"));
                                double end = item.getEndMillis();
                                end *= 1000;
                                area.setAttribute("end", (end + "s"));
                                area.setAttribute("id", ("a" + item.getContent()));
                            }
                        }
                        for (Audio audio : audios) {
                            if (("a" + audio.getOriginalId()).equals(area.getAttribute("id"))) {
                                AnnotationItem item = annotation.getAnnotationById(audio.getId());
                                double start = item.getStartMillis();
                                start *= 1000;
                                area.setAttribute("begin", (start + "s"));
                                double end = item.getEndMillis();
                                end *= 1000;
                                area.setAttribute("end", (end + "s"));
                                area.setAttribute("id", ("a" + item.getContent()));
                            }
                        }
                    }
                }
            }
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);

            File eventDescription = new File(this.root + File.separator + "eventDescription2.ncl");
            log.log(Level.INFO, "{0}{1}eventDescription2.ncl", new Object[]{this.root, File.separator});
            eventDescription.createNewFile();

            StreamResult result = new StreamResult(eventDescription);

            transformer.transform(source, result);
        } catch (SAXException ex) {
            Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Save the srt file changes
     * @param path root path where the srt will be saved using the {@link Annotation#originalId} as name
     */
    public void saveSrt(String path) {
        log.info("Saving srt annotations");
        FileWriter fw = null;
        try {
            File f = new File(path + File.separator + annotation.getOriginalId() + ".srt");
            f.createNewFile();
            String data = new String();
            int count = 1;
            for (AnnotationItem item : annotation.getAnnotations()) {
                boolean ok = false;
                for (Video video : videos) {
                    if (video.getId() == item.getId()) {
                        ok = true;
                    }
                }
                for (Audio audio : audios) {
                    if (audio.getId() == item.getId()) {
                        ok = true;
                    }
                }
                if (!ok) {
                    data += (count++) + "\n" + item.getStart() + " --> " + item.getEnd() + "\n" + item.getContent() + "\n\n";
                }
            }
            fw = new FileWriter(f);
            fw.write(data);
        } catch (IOException ex) {
            Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                log.info("Save completed successfully");
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(MediaDescriptor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
