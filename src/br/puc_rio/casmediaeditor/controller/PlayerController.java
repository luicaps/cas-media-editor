package br.puc_rio.casmediaeditor.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import jfx.messagebox.MessageBox;

/**
 * Portion of the GUI that holds the player and all its control. Here is the
 * first point that decides what is going to be the profile for the player
 * regarding to the GUI
 *
 * @author Luiz
 */
public class PlayerController implements Initializable {

    /**
     * Logger
     */
    private static final Logger log = Logger.getLogger(PlayerController.class.getName());

    /**
     * Variables linked to the GUI elements from FXML.
     */
    @FXML
    HBox MenuHBox;
    @FXML
    HBox PlayerHBox;
    @FXML
    Slider timeSlider;
    @FXML
    Pane video1;
    @FXML
    Pane video2;
    @FXML
    Slider volumeSlider;
    @FXML
    Button play_pause;
    @FXML
    Button full_screen;
    @FXML
    Button open_file;
    @FXML
    Label playTime;
    @FXML
    Pane timelineView;

    /**
     * The Synchronized Player. This instance controls the ongoing of the
     * various media. It also defines the media to be used and how it is
     * presented in the center of the Player screen.
     */
    SyncPlayer player;

    /**
     * Initialization method called by JavaFX when all the default and GUI
     * objects instances are already created. This method is used to prepare the
     * scene.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        log.info("Preparing Player GUI");
        //Mark the GUI objects to grow with the window's resize event
        HBox.setHgrow(timeSlider, Priority.ALWAYS);
        full_screen.setDisable(true);
        setGUIAsInvalid();
        log.info("Player ready.");
    }

    /**
     * Button action to control the play and pause of the videos
     *
     * @param event
     */
    @FXML
    private void playPauseAction(ActionEvent event) {
        log.info("Play/Pause called");
        player.playPause();
    }

    /**
     * Button action to open the Event recorded on CAS
     *
     * @param event
     */
    @FXML
    private void openFileAction(ActionEvent event) {
        log.info("Open File called");
        Stage stage = (Stage) play_pause.getScene().getWindow();
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("CAS Events");
        File defaultDirectory = new File("c:");
        chooser.setInitialDirectory(defaultDirectory);
        log.info("Showing file chooser dialog");
        File selectedDirectory = chooser.showDialog(stage);
        log.info("Creating Sync Player instance");
        //TODO Implement better preparation for event, analysing the media and calling the correct instance, or giving a feedback if event is not good
        player = new DefaultSyncPlayer(new MediaDescriptor(selectedDirectory.getAbsolutePath(), this), timelineView, PlayerHBox, timeSlider, volumeSlider, playTime, play_pause);
        setGUIAsValid();
        open_file.setDisable(true);

        log.info("Creating annotation table");
        try {
            final Stage secondaryStage = new Stage(StageStyle.UTILITY);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/puc_rio/casmediaeditor/view/tableview/TableView.fxml"));
            Parent secondaryRoot = (Parent) loader.load();
            TableViewController controller = (TableViewController) loader.getController();
            controller.putData(player.getMediaDescriptor());
            controller.setPlayer(player);
            Scene secondaryScene = new Scene(secondaryRoot);
            secondaryStage.setScene(secondaryScene);
            secondaryStage.setResizable(false);
            secondaryStage.setX(750);
            secondaryStage.setY(50);
            secondaryStage.initOwner(stage);
            stage.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent onClosing) {
                    secondaryStage.hide();
                }
            });
            secondaryStage.show();
            log.info("Annotation table created");
        } catch (IOException ex) {
            Logger.getLogger(PlayerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void handleSaveButtonAction(ActionEvent event) {
        log.info("Save called");
        player.getMediaDescriptor().saveEvent();
    }

    public void fileErrorMessage() {
        MessageBox.show(MenuHBox.getScene().getWindow(), "Não foi possível encontrar um evento corretamente gravado pelo CAS para reprodução.",
                "Não foi possível encontrar dados do CAS", MessageBox.OK);
    }

    /**
     * Change the state of the GUI as invalid, disabling the interaction
     * elements of the GUI
     */
    protected void setGUIAsInvalid() {
        log.info("Changing GUI to invalid");
        play_pause.setDisable(true);
        timeSlider.setDisable(true);
        volumeSlider.setDisable(true);
    }

    /**
     * Change the sate of the GUI as valid, enabling the interaction elements of
     * the GUI
     */
    protected void setGUIAsValid() {
        log.info("Changing GUI to valid");
        play_pause.setDisable(false);
        timeSlider.setDisable(false);
        volumeSlider.setDisable(false);
    }

}
